#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
import os
import sys
import logging
import subprocess

from pathlib import Path

log = logging.getLogger('')

class A64Recompiler:
  """ This class aims to abstract the setup of an AARCH64 CPU and be in sync
  with a64transpiler ccode/core.

  The code should be initially disassembled into code and data blocks so that
  this class can reassemble everything into a C file and compile it again.
  """
  ARCH_WASM = 'wasm'
  ARCH_NATIVE = 'native'

  def __init__ (self, blocks = None):
    self.blocks = blocks if blocks else None
    self.code2insert = []
    self.stackSizeInKb = 1024 # 1MB by default

  def addBlock (self, codeBlock):
    self.blocks.append (codeBlock)

  def setStackSizeInKb (self, sizeInKb):
    self.stackSizeInKb = sizeInKb

  def invokeRaw(self, rawCode):
    """ Insert given raw code in the main method
    """
    self.code2insert.append (rawCode)

  def invoke(self, functionName, *args):
    """ Insert a function call passing as first argument the CPU and given
    set of arguments
    """
    argsExpanded = ', ' + ', '.join(args) if args else ''
    code = f'{functionName}(&cpu{argsExpanded});'
    self.code2insert.append (code)

  def _dumpNiceComment(self, file, comments):
    """ Just a wrapper to print a nice comment. Comment can be a string or a
    list of strings
    """
    if isinstance(comments, str):
      comments = [comments]
    file.write (f'// {"-"*76}\n')
    for comment in comments:
      file.write (f'// {comment}\n')
    file.write (f'// {"-"*76}\n')

  def _dumpIncludes(self, file):
    """ Dump required include files for the A64 code to compile.
    Required includes are
    """
    file.write ('#include <stdlib.h>\n')
    file.write ('#include "a64_all.c"\n')
    file.write ('\n')
    return

  def _dumpCodeDeclarations (self, file):
    """ Function declarations are used so we can reference any method from
      anywhere else
    """
    self._dumpNiceComment(file, 'function declarations')
    for block in self.blocks:
      if not block.isCode():
        continue
      file.write (f'void {block.name}(a64cpu_t *cpu);\n')

    file.write ('\n')

  def _dumpData(self, file):
    """ Dump data values and initialization here
    """
    self._dumpNiceComment(file, 'data')

    for block in self.blocks:
      if not block.isData():
        continue

      file.write ('\n'.join (block.ccode))
      file.write ('\n\n')

    file.write ('\n')
    return

  def _dumpCode(self, file):
    self._dumpNiceComment(file, 'code')

    for block in self.blocks:
      if not block.isCode():
        continue

      file.write (f'void {block.name}(a64cpu_t *cpu) {{\n')
      file.write ('\n'.join ([f'  {line}' for line in block.ccode]))
      file.write ('\n}\n\n')

    return

  def _dumpMain(self, file):
    """ Main initialization to invoke all methods desired
    """
    code2insert = self.code2insert if self.code2insert else [ 'entry_point(&cpu);' ]

    # main method: initialization
    self._dumpNiceComment(file, 'main method')

    template = f"""
int main(int argc, char *argv[]) {{
  a64cpu_t cpu;
  memset(&cpu, 0, sizeof(cpu));

  // creates the stack and assigns SP to the end of it
  cpu.stack_size = {self.stackSizeInKb*1024};
  cpu.stack_base = (uint8_t*)calloc({self.stackSizeInKb}, 1024);
  cpu.sp = (uint64_t)(cpu.stack_base + cpu.stack_size);

  <!EXPAND_CODE_HERE!>

  free (cpu.stack_base);
  cpu.stack_base = NULL;

  return 0;
}}"""

    file.write (
      template.replace ('<!EXPAND_CODE_HERE!>', '\n  '.join(self.code2insert))
    )

    file.write ('\n\n')
    return

  def _dumpIgnored(self, file):
    """ Dump ignorable data here
    """
    self._dumpNiceComment(file, 'sections to ignore')

    for block in self.blocks:
      if not block.isIgnored():
        continue

      file.write ('\n'.join (block.ccode))
      file.write ('\n\n')

    file.write ('\n')
    return

  def dumpCCode(self, file, verbose = False):
    """ Dump the whole CCode
    """
    self._dumpNiceComment(file, [
      'IMPORTANT: this file has been generated automatically'
    ])

    self._dumpIncludes(file)
    self._dumpCodeDeclarations(file)
    self._dumpData(file)
    self._dumpCode(file)
    self._dumpMain(file)

    if verbose:
      self._dumpIgnored(file)
    return

  def invokeCCompilerNative(self, targetFile, cSourceFile):
    """ Compile given C code file to native using GCC
    """
    a64TranspilerCCodeDir = os.path.normpath (
      os.path.join(os.path.dirname(__file__), 'a64transpiler/ccode/')
    )

    result = subprocess.run(
      [
        'gcc',
        '-O3',
        '-o', targetFile,
        str(cSourceFile),
        '-I', a64TranspilerCCodeDir
      ],
      cwd = os.path.dirname (cSourceFile),
      capture_output = True
    )

    if result.returncode != 0:
      print ("ERROR: ", result.args, ":", result.stdout.decode('utf-8'))
      print ("ERROR: ", result.args, ":", result.stderr.decode('utf-8'))

    return (result.returncode == 0)

  def invokeCCompilerWasm(self, targetFile, cSourceFile):
    """ Compile given C code file to native using emscripten compiler
    """
    a64TranspilerCCodeDir = os.path.normpath (
      os.path.join(os.path.dirname(__file__), 'a64transpiler/ccode/')
    )

    result = subprocess.run(
      [
        'emcc',
        '-O3',
        '-s', 'WASM=1',
        #'-s', 'STANDALONE_WASM',
        '-o', targetFile,
        str(cSourceFile),
        '-I', a64TranspilerCCodeDir
      ],
      cwd = os.path.dirname (cSourceFile),
      capture_output = True
    )

    if result.returncode != 0:
      print ("ERROR:", result.args, ":", result.stdout.decode('utf-8'))
      print ("ERROR:", result.args, ":", result.stderr.decode('utf-8'))

    return (result.returncode == 0)

  def invokeCCompiler(self, targetFile, cSourceFile, targetArch = ARCH_WASM):
    """ Invoke C compiler over given file. It can compile the file to native
    architecture or to WASM.
    """
    if targetArch == A64Recompiler.ARCH_WASM:
      return self.invokeCCompilerWasm(targetFile, cSourceFile)

    elif targetArch == A64Recompiler.ARCH_NATIVE:
      return self.invokeCCompilerNative(targetFile, cSourceFile)

    raise Exception (f'Invalid architecture: {targetArch}')

  def rebuild(self, tempDir, targetArch = ARCH_WASM, baseName = 'a64transpiled'):
    """ This is a generic method that does all steps needed to rebuild the
    original blocks.
    """
    extension = '.wasm' if targetArch == A64Recompiler.ARCH_WASM else ''
    targetFile = Path(tempDir) / f'{baseName}{extension}'
    cSourceFile = Path(tempDir) / f'{baseName}.c'
    with open(cSourceFile, 'wt') as file:
      self.dumpCCode(file)

    log.debug (f'Compile [{targetArch}]: {targetFile}')

    if not self.invokeCCompiler(
      targetFile,
      cSourceFile,
      targetArch
    ):
      return None

    return targetFile

  def runProgram (self, targetFile, targetArch = ARCH_WASM):
    """ Run program and returns the tuple (retcode, stdout, stderr)
    """
    if targetArch == A64Recompiler.ARCH_WASM:
      proc = subprocess.run (
        [ 'wasmer', 'run', targetFile ],
        cwd = os.path.dirname(targetFile),
        capture_output = True
      )
      return (proc.returncode, proc.stdout, proc.stderr)

    elif targetArch == A64Recompiler.ARCH_NATIVE:
      proc = subprocess.run (
        [ targetFile ],
        cwd = os.path.dirname(targetFile),
        capture_output = True
      )
      return (proc.returncode, proc.stdout, proc.stderr)

    raise Exception (f'Invalid architecture: {targetArch}')

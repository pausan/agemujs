#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
# ElfLib.py provides high-level methods to manipulate elf object files,
# object sections, symbols and relocations. It relies on elftools module
# to parse all information.
#
# The intent of this wrapper is to make it easier to represent the internal
# structure in a way that is easier to interact with and manipulate later on.
#
#  - Symbols have a reference to sections or instructions they belong to
#  - Symbols have a reference to each relocation referencing to them
#  - Relocations have a reference to symbols
#  - Relocations have a reference to sections or instructions they belong to
#  - Sections have a reference to objects they belong to
#  - All offsets are relative within their section or instruction
#  - Instruction contain symbols and relocations referencing other places
#
#  - _sectionOwner_ is always used to refer to parent sections. Both symbols
#    and relocations always belong to a section.
#
#  - _instrOwner_ sometimes symbols and relocs might also belong to a
#    instruction (apart of section), in those cases, we hold the owner.
#    We keep owners separate because it allows more flexibility when working
#    with them.
#

from collections import defaultdict
from elftools.elf.elffile import ELFFile
from elftools.elf import constants as elfconstants

import capstone

class Instr:
  """ Represents a single instruction. It is just a wrapper over capstone
  instruction with a couple of extras, like instruction address, symbol
  and relocation information (if available)
  """
  def __init__ (self, section, capstoneInstr):
    """ Initializes the object with an address and a capstone instruction
    """
    assert (isinstance(section, Section))
    self.sectionOwner = section
    self.csInstr = capstoneInstr
    self.symbols = []
    self.relocs  = []
    return

  def hasRelocs(self):
    """ Returns True when this method has no relocations
    """
    return len(self.relocs) > 0

  @property
  def addr(self):
    return self.csInstr.address

  @property
  def size(self):
    return self.csInstr.size

  @property
  def mnemonic(self):
    return self.csInstr.mnemonic

  @property
  def args(self):
    return self.csInstr.operands

  @property
  def nargs(self):
    return len(self.csInstr.operands)

  @property
  def writeback(self):
    return self.csInstr.writeback

  def regName(self, regId):
    """ Returns register name
    """
    return self.csInstr.reg_name (regId)

  def __call__(self, *args, **kwargs):
    """ bypass all methods
    """
    return self.csInstr (*args, **kwargs)

  def __repr__ (self):
    instr = self.csInstr
    #hexbytes = ' '.join(['%02x' % b for b in reversed(instr.bytes)])
    hexbytes = ' '.join(['%02x' % b for b in instr.bytes])

    relocs = []
    for reloc in self.relocs:
      relocs.append (repr(reloc))

    if relocs:
      relocs = '; rel[' + ';'.join(relocs) + ']'
    else:
      relocs = ''

    # (s={instr.size})
    s = [
      f'{instr.address:08x}: {hexbytes}   {instr.mnemonic:6s}  {instr.op_str:24s}{relocs}'
    ]

    return '\n'.join(s)

class Symbol:
  """ Represents a symbol within the file, usually symbols belong to
  specific sections and can be referenced by relocations.

  _section_ field refers to section owner
  _instr_ field refers to instruction owner (within section)
  """
  def __init__ (self, section, name, addr, bind, type, visibility):
    self.sectionOwner = section
    self.instrOwner = None
    self.name = name
    self.addr = addr
    self.bind = bind # LOCAL | GLOBAL | ... | WEAK | ...
    self.type = type # NONE | OBJECT | FUNC | SECTION | FILE | ...
    self.visibility = visibility  # DEFAULT | INTERNAL | ...

    # list of reloc references pointing to this symbol
    self.relocReferences = []

  @property
  def instrAddr(self):
    """ Address (offset) of symbol within given instruction.
    """
    if not self.instrOwner:
      return None
    return self.addr - self.instrOwner.addr

  def isPublic(self):
    return (self.bind == 'STB_GLOBAL')

  def __repr__(self):
    s = [
      f'{self.addr:08x}: {self.name:32s} [{self.bind}, {self.type}, {self.visibility}]'
    ]
    return '\n'.join (s)

class Reloc:
  """ Represents a relocation contained in a section, starting from given
  offset and referencing given symbol (of current or other section).

  Offset is always relative to its owner (e.g on a section it will be relative
  to the section start, but on an instruction it will be relative to the
  instruction start)

  _addend_ will be used to patch symbols.
  """
  def __init__(self, section, offset, symbol, type, addend = 0):
    self.sectionOwner = section
    self.instrOwner = None
    self.offset = offset
    self.symbol = symbol
    self.type = type
    self.addend = addend

    self.symbol.relocReferences.append (self)
    return

  @property
  def instrOffset(self):
    """ Address (offset) of relocation within given instruction.
    """
    if not self.instrOwner:
      return None
    return self.offset - self.instrOwner.addr

  def __repr__(self):
    # TODO: expand flags
    sflags = []

    # FIXME!
    # for k, v in elfenums.ENUM_RELOC_TYPE_AARCH64.__dict__.items():
    #   if k.startswith ('SHR_') and (self.flags & v) == v:
    #     sflags.append (k[4:].lower())

    s = [
      f'off={self.offset:08x}: {self.symbol.name:16s} (+{self.addend}) [{self.type}]'
    ]
    return '\n'.join (s)

class SymbolMap:
  """ SymbolMap represents a group of symbols that can be addressed.

  It always belong to a section.
  """
  def __init__ (self, section):
    assert (isinstance (section, Section))
    self.sectionOwner = section
    self.nameLookup = {}
    self.addrLookup = defaultdict(list)
    self.symbols = []

  def has(self, name):
    """ Checks if symbol map contains a symbol with given name
    """
    return name in self.nameLookup

  def hasAny(self, nameList):
    """ Checks if symbol map contains any of the symbols in given list
    """
    if not nameList:
      return False

    return any([self.has(name) for name in nameList])

  def lookup(self, nameOrAddr, default = None):
    """ Lookup given symbol by name or address. When found it will return
    the symbol or list of symbols, and when not, will return given default
    value or None if no default value is specified.

    Examples:

      >>> symbols.lookup (0x100)
      [ Symbol('name1', ...), Symbol('name2', ...)]

      >>> symbols.lookup (0x123)
      None

      >>> symbols.lookup (0x123, [])
      []

      >>> symbols.lookup ('_main')
      Symbol('_main', ...)

      >>> symbols.lookup ('__missing__symbol__', Symbol('asdf'))
      Symbol('asdf')

    """
    if isinstance (nameOrAddr, int):
      return self.addrLookup.get(nameOrAddr, default)
    return self.nameLookup.get(nameOrAddr, default)

  def add (self, symbol):
    # assert works, however the symbol can also belong to an instruction
    # whose owner is the section (self.sectionOwner)
    assert (symbol.sectionOwner == self.sectionOwner)
    self.nameLookup[symbol.name] = symbol
    self.addrLookup[symbol.addr].append(symbol)
    self.symbols.append (symbol)
    return

  def empty(self):
    return len(self.symbols) == 0

  def size(self):
    return len(self.symbols)

  def items(self):
    return self.symbols

  def __iter__ (self):
    return self.symbols.__iter__()

class Section:
  """ Models a code section with data, symbols and relocs
  """
  def __init__(self, index, name, elfSection = None):
    self.clear()

    self.elfSection = elfSection
    self.index = index
    self.name = name
    return

  def clear(self):
    self.elfSection = None
    self.index = 0
    self.name = ''
    self.data = b''
    self.type = None
    self.flags = 0
    self.addr = 0
    self.offset = 0
    self.size = 0
    self.link = 0
    self.info = 0
    self.addralign = 0
    self.entsize = 0

    self.instrs = []
    self.symbols = SymbolMap(self)
    self.relocs = []
    return

  def parseElfSection (self, elfSection):
    """ Parses information from given ELF section
    """
    self.name      = elfSection.name
    self.data      = elfSection.data()
    self.type      = elfSection['sh_type']        # SHT_PROGBITS
    self.flags     = elfSection['sh_flags']       # 6
    self.addr      = elfSection['sh_addr']        # 0
    self.offset    = elfSection['sh_offset']      # 64
    self.size      = elfSection['sh_size']        # 160
    self.link      = elfSection['sh_link']        # 0
    self.info      = elfSection['sh_info']        # 0
    self.addralign = elfSection['sh_addralign']   # 4
    self.entsize   = elfSection['sh_entsize']     # 0

    # NOTE: symbols and relocs need to be added to the object from a
    #       higher-level entity (such as Object parser)
    return

  def hasFlag (self, flag):
    """ Returns TRUE if given flag is set
    """
    return ((self.flags & flag) == flag)

  def hasWriteFlag(self):
    return self.hasFlag (elfconstants.SH_FLAGS.SHF_WRITE)

  def isBss(self):
    """ Returns True if this section contains uninitialized data
    """
    return (
      (self.type == 'SHT_NOBITS')
      and self.hasFlag (elfconstants.SH_FLAGS.SHF_ALLOC)
    )

  def isData(self):
    return (
      (self.type == 'SHT_PROGBITS')
      and (not self.hasFlag (elfconstants.SH_FLAGS.SHF_EXECINSTR))
    )

  def hasCode (self):
    """ Returns _True_ when this section contains executable code
    """
    return self.hasFlag (elfconstants.SH_FLAGS.SHF_EXECINSTR)

  def disassemble (self, disassembler):
    """ Disassembles current section and associates symbols and relocations to
    the right instruction.

    Associates a new ownership on symbols and relocs to instructions.
    """
    self.reduceSymbols()

    # TODO: create sorted list by offset, and get an index to it so we have next
    #       offset available to avoid the loop in next section
    relocsLookup = {}
    for reloc in self.relocs:
      relocsLookup[reloc.offset] = reloc

    relocsOffsets = []
    for csInstr in disassembler.disasm(self.data, self.addr):
      instr = Instr(self, csInstr)
      instr.symbols = self.symbols.lookup (instr.addr, [])

      # change symbol ownership
      for symbol in instr.symbols:
        symbol.instrOwner = instr

      # TODO: suboptimal, but readable
      for offset in range(instr.addr, instr.addr + instr.size):
        reloc = relocsLookup.get (offset, None)
        if reloc:
          reloc.instrOwner = instr
          instr.relocs.append (reloc)
          relocsOffsets.append (instr.addr)

      self.instrs.append (instr)

    # NOTE: symbols are not cleared, since the symbol map can still be used as
    #       a lookup

    # remove all relocs that have been associated to instructions
    # (in theory all of them should have)
    assert (relocsOffsets == sorted(relocsLookup.keys()))
    self.relocs = []

  def reduceSymbols(self):
    """ Usually there is more than one symbol at the same offset. The idea of
    this method is to simplify symbols and leave only one. All the other
    symbols will be replaced.
    """
    pass # FIXME!

  def __repr__ (self):
    s = []
    s.append (f'Section {self.index:03d}: {self.name:16s}')

    # expand flag names
    sflags = []
    for k, v in elfconstants.SH_FLAGS.__dict__.items():
      if k.startswith ('SHF_') and (self.flags & v) == v:
        sflags.append (k[4:].lower())

    # TODO: be more verbose!
    s.append (f'  type      : {self.type}')
    s.append (f'  flags     : {self.flags} {sflags}')
    s.append (f'  addr      : {self.addr}')
    s.append (f'  offset    : {self.offset}')
    s.append (f'  size      : {self.size}')
    s.append (f'  link      : {self.link}')
    s.append (f'  info      : {self.info}')
    s.append (f'  addralign : {self.addralign}')
    s.append (f'  entsize   : {self.entsize}')

    if not self.symbols.empty():
      s.append (f'  symbols   :')
      s.append ('')
      for symbol in self.symbols:
        s.append (f'    0x{repr(symbol)}')
      s.append ('')

    if self.hasCode() and len(self.instrs) > 0:
      s.append (f'  code      :')
      for instr in self.instrs:
        if len(instr.symbols):
          s.append ('')

        for symbol in instr.symbols:
          s.append (f'    0x{repr(symbol)}')

        s.append (f'    0x{repr(instr)}')
      s.append ('')

    else:
      sname = 'code' if self.hasCode() else 'data'
      if len(self.data) == 0:
        s.append (f'  {sname}      : (empty)')
      else:
        s.append (f'  {sname}      :')
        s.append ('')
        for offset in range(0, len(self.data), 16):
          hexbytes = ' '.join(['%02x' % c for c in self.data[offset:offset+16]])
          chars = ''.join(['%c' % c if ('%c' % c).isprintable() else '.' for c in self.data[offset:offset+16]])
          s.append (f'    0x{self.addr + offset:08x}: {hexbytes:48s} {chars}')
        s.append ('')


    if self.relocs:
      s.append (f'  relocs    :')
      s.append ('')
      for reloc in self.relocs:
        s.append (f'    0x{repr(reloc)}')
      s.append ('')

    return '\n'.join(s)

class Object:
  """ Represent a single object file. Object files are the container class
  for sections, symbols and relocations.

  NOTE: disassembler only works with AARCH64 (on purpose)

  Example:

    >>> obj = elflib.Object()
    >>> obj.load ('myfile_aarch64.o')
    >>> obj.disassemble()

  """
  def __init__(self):
    self.filePath = None
    self.elfFile = None
    self.sections = []
    self.symbols = []

  def clear(self):
    """ Clears everything from the internal representation
    """
    self.filePath = None
    self.elfFile = None

  def load(self, filePath):
    """ Load given object file path into
    """
    self.clear()
    self.filePath = filePath

    with open(filePath, 'rb') as f:
      self.elfFile = ELFFile(f)

      for sectionIndex, elfSection in enumerate(self.elfFile.iter_sections()):
        section = Section (sectionIndex, elfSection.name, elfSection)
        section.parseElfSection (elfSection)
        self.sections.append (section)

      # Let's create two 'virtual' sections, for the sake of completness
      section = Section (len(self.sections), '__misc_symbols__', None)
      self.sections.append(section)

      section = Section (len(self.sections), '__external_symbols__', None)
      self.sections.append(section)

      # symbol table needs to be available for relocations to work
      for section in self.sections:
        if section.type == 'SHT_SYMTAB':
          self._parseSymbolTable (section.elfSection)

      # finally parse relocations
      for section in self.sections:
        if section.type == 'SHT_RELA':
          self._parseRelocTable (section.elfSection)

    return

  def _parseSymbolTable(self, elfSymbolSection):
    """ Parse symbol map from ELF symbol table
    """
    miscSection = self.sections[-2]
    externalSection = self.sections[-1]

    self.symbols = []
    for elfSymbol in elfSymbolSection.iter_symbols():
      sectionIndex = elfSymbol['st_shndx']

      # IMPORTANT: all symbols must be present in self.symbols so that we
      #            can lookup symbols in the relocations by using the symbol
      #            index. If any symbol is removed, relocations won't be
      #            addressing the right symbols. Sections are important to
      #            manipulate symbols and make everything stay consistent.
      symbolSection = miscSection

      if isinstance(sectionIndex, int):
        symbolSection = self.sections[sectionIndex]

      elif sectionIndex == 'SHN_UNDEF':
        if elfSymbol.entry.st_info.bind == 'STB_GLOBAL':
          symbolSection = externalSection

      elif sectionIndex == 'SHN_ABS':
        if (
          (elfSymbol.entry.st_info.bind == 'STB_LOCAL')
          or (elfSymbol.entry.st_info.type == 'STT_FILE')
        ):
          symbolSection = miscSection

      name = elfSymbol.name
      address = elfSymbol.entry.st_value

      # add a "virtual" name to know what we are referencing
      if not name:
        assert (address == 0)

        if symbolSection.name:
          name = symbolSection.name
        else:
          name = f'$_section_{sectionIndex}'

      symbol = Symbol(
        symbolSection,
        name,
        address,
        elfSymbol.entry.st_info.bind,
        elfSymbol.entry.st_info.type,
        elfSymbol.entry.st_other.visibility
      )

      symbolSection.symbols.add (symbol)
      self.symbols.append (symbol)

    return

  def _parseRelocTable (self, elfRelocSection):
    """ Parse reloc section information
    """
    assert (elfRelocSection is not None)

    # this reloc table references given section
    section = self.sections [ elfRelocSection['sh_info'] ]

    for reloc in elfRelocSection.iter_relocations():
      # get symbol from the index in the symbol table
      targetSymbol = self.symbols[reloc['r_info_sym']]
      assert (targetSymbol is not None)

      # print (reloc, section.index, targetSymbol.name)
      sectionOffset = reloc['r_offset']

      reloc = Reloc(
        section,
        sectionOffset,
        targetSymbol,
        reloc['r_info_type'],
        reloc['r_addend'] if reloc.is_RELA() else 0
      )

      section.relocs.append (reloc)
    return

  def disassemble(self):
    """ Once the main contents of the elf file have been parsed, and all
    information is available (section headers, symbol table and relocs)
    we can disassemble instructions from all sections and place all symbols
    and relocs appropriately
    """
    machine = self.elfFile['e_machine']
    if machine != 'EM_AARCH64':
      raise Exception ('Cannot disassemble {machine} (only AARCH64 is supported)')

    disassembler = capstone.Cs(capstone.CS_ARCH_ARM64, capstone.CS_MODE_ARM)
    disassembler.detail = True

    for section in self.sections:
      if section.hasCode():
        section.disassemble(disassembler)

    return

  def __repr__(self):
    s = []
    s.append (f'File: {self.filePath}')
    for section in self.sections:
      s.append(repr(section))
      s.append('')
    return '\n'.join(s)

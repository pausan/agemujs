#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
from . import a64cpu
from capstone import arm64 as csarm64

def isRegisterW32(regIndex):
  return regIndex in a64cpu.ARM64_W32BIT_REG

def isRegisterX64(regIndex):
  return regIndex in a64cpu.ARM64_X64BIT_REG

def capstoneShowInstr(instr):
  rbytes = instr.bytes[:]
  rbytes.reverse()
  print(f'0x{instr.address:08x}: {rbytes.hex():8s}   {instr.mnemonic:6s}  {instr.op_str:32s}    (s={instr.size})')

def capstoneShowInstrEx(instr):
  capstoneShowInstr(instr)

  if len(instr.regs_read) > 0:
    print("  Implicit registers read: "),
    for r in instr.regs_read:
      print("%s " % instr.reg_name(r))
    print("")

  if len(instr.groups) > 0:
    print("  This instruction belongs to groups:"),
    for g in instr.groups:
      print("%u" %g)
    print("")

  if len(instr.operands) > 0:
    print("  Number of operands: %u" %len(instr.operands))
    c = -1
    for i in instr.operands:
      c += 1
      if i.type == csarm64.ARM64_OP_REG:
        print("    operands[%u].type: REG = %s" %(c, instr.reg_name(i.value.reg)))

      if i.type == csarm64.ARM64_OP_IMM:
        print("    operands[%u].type: IMM = 0x%x" %(c, i.value.imm))

      if i.type == csarm64.ARM64_OP_CIMM:
        print("    operands[%u].type: C-IMM = %u" %(c, i.value.imm))

      if i.type == csarm64.ARM64_OP_FP:
        print("    operands[%u].type: FP = %f" %(c, i.value.fp))

      if i.type == csarm64.ARM64_OP_MEM:
        print("    operands[%u].type: MEM" %c)
        if i.value.mem.base != 0:
          print("      operands[%u].mem.base: REG = %s" %(c, instr.reg_name(i.value.mem.base)))
        if i.value.mem.index != 0:
          print("      operands[%u].mem.index: REG = %s"  %(c, instr.reg_name(i.value.mem.index)))
        if i.value.mem.disp != 0:
          print("      operands[%u].mem.disp: 0x%x" %(c, i.value.mem.disp))

      if i.shift.type != csarm64.ARM64_SFT_INVALID and i.shift.value:
        print("      Shift: type = %u, value = %u" %(i.shift.type, i.shift.value))

      if i.ext != csarm64.ARM64_EXT_INVALID:
        print("      Ext: %u" %i.ext)

  if instr.writeback:
    print("  Write-back: True")

  if not instr.cc in [csarm64.ARM64_CC_AL, csarm64.ARM64_CC_INVALID]:
    print("  Code condition: %u" %instr.cc)

  if instr.update_flags:
    print("  Update-flags: True")

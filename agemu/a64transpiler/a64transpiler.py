#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#

# make all instructions available on this namespace

from . import a64cpu, a64util, a64fmt
from .a64block import A64Block

from capstone import arm64 as csarm64

def _formatInstruction (instr):
  """ Generic instruction formatter

  It formats the instruction as a C method call with the following parts:

    <mnemonic>_<operands>[memsuffix] (<arguments>);

  Each operand is represented by a single letter, and can take the following
  values:
    - w: w register
    - x: x register
    - i: immediate value
    - m: memory reference

  memsuffix only exists if 'm' is present, and can take the following values:
    - ridx: extra register index
    - off:  offset to be added to memory base register
    - pre:  offset to be added to memory base register before accessing memory
    - post: offset to be added to memory base register after accessing memory

  Examples:
    >>> instr (Instr.parse('ret'))
    'a64_ret (cpu);'

    >>> instr (Instr.parse('add x0, x0, #0'))
    'a64_add_xxi (cpu, &(cpu->r0), &(cpu->r0), 0x00000000);'

    >>> instr (Instr.parse('stp x29, x30, [sp, #-0x10]!'))
    'a64_stp_xxm_pre (cpu, &(cpu->r29), &(cpu->r30), &(cpu->sp), -0x00000010);'

    >>> instr (Instr.parse('ldp w29, w30, [sp], #0x10'))
    'a64_ldp_wwm_post (cpu, &(cpu->r29), &(cpu->r30), &(cpu->sp), -0x00000010);'
  """
  name = ['a64', instr.mnemonic]
  argname = []
  memsuffix = []
  args = ['cpu']

  # used to identify postindex addressings
  lastArg = None

  for arg in instr.args:
    if arg.type == csarm64.ARM64_OP_REG:
      if a64util.isRegisterW32(arg.value.reg):
        argname.append('w')
      else:
        argname.append('x')

      args.append (a64fmt.reg(arg.value.reg))

    elif arg.type == csarm64.ARM64_OP_MEM:
      argname.append('m')

      if arg.value.mem.base != 0:
        args.append (a64fmt.reg(arg.value.mem.base))

      if arg.value.mem.index != 0:
        memsuffix.append ('ridx')
        args.append (a64fmt.reg(arg.value.mem.index))

      if arg.value.mem.disp != 0:
        if instr.writeback:
          memsuffix.append('pre')
          args.append(a64fmt.address(arg.value.mem.disp, instr.relocs))
        else:
          memsuffix.append('off')
          args.append(a64fmt.address(arg.value.mem.disp, instr.relocs))

    elif arg.type == csarm64.ARM64_OP_IMM:
      # postindex memory operations have an immediate after MEM operation
      if lastArg and lastArg.type == csarm64.ARM64_OP_MEM:
        assert (instr.writeback)
        memsuffix.append ('post')
      else:
        argname.append ('i')

      args.append(a64fmt.imm(arg.value.imm, instr.relocs))

    elif arg.type == csarm64.ARM64_OP_CIMM:
      # imm(arg.value.imm, instr.relocs)?
      raise Exception ('Not implemented!')

    elif arg.type == csarm64.ARM64_OP_FP:
      # arg.value.fp ?
      raise Exception ('Not implemented!')

    else:
      raise Exception ('Operand Type Not Supported: ', arg.type)

    if arg.shift.type != csarm64.ARM64_SFT_INVALID and arg.shift.value:
      # arg.shift.type, arg.shift.value
      raise Exception ('Not implemented!')

    if arg.ext != csarm64.ARM64_EXT_INVALID:
      # i.ext
      raise Exception ('Not implemented!')

    lastArg = arg

  # merge 1 letter operands
  if argname:
    name.append (''.join(argname))
  name.extend (memsuffix)

  return f'{"_".join (name)} ({", ".join(args)});'

def transpileInstruction (instr, verbose = True):
  """ Transforms an AARCH64/ARM64 instruction or virtual instruction into
  a call to C code. One instruction of input should produce one line of output.

  Examples:

    >>> transpileInstruction (instr)
    "a64_movz_ri (cpu, &(cpu->r0), 0x0000002a);"

    >>> transpileInstruction (instr, verbose = True)
    "a64_movz_ri (cpu, &(cpu->r0), 0x0000002a);   // movz r0, #0x2a"

  """
  code = _formatInstruction(instr)

  if verbose:
    original = f' // {instr}'
    return f'{code:90s}{original}'

  return code

def transpileFunction(section, verbose = True):
  """ Let's assume the whole section passed as parameter is a function.
  A function will be invoked by its public symbol and will return normally.

  Returns a string containing the transpiled method
  """
  pubSymbols = [s for s in section.symbols if s.isPublic() and s.addr == 0]

  # code, with more than 1 instruction and only one public symbol on instruction 0
  assert (
    section.hasCode()
    and (len(section.instrs) > 0)
    and (len(pubSymbols) == 1)
  )

  symbol = pubSymbols[0]

  # TODO: rearrange instructions so that we combine things like movz and movk
  #       into one "virtual" instruction and/or other bitwise operations
  #       move instructions that change flags closer to when they are used, ...
  #       any optimization should be done here
  #       create virtual instructions so they are properly transpiled later

  code = []
  for instr in section.instrs:
    code.append ( transpileInstruction(instr, verbose) )

  return A64Block(A64Block.TYPE_CODE, symbol.name, code)


def transpileData(section, verbose = True):
  """ We need to produce a data block from given section.

  Code might try access data anywhere between the section but will certainly
  not trespass different sections.

  There are three possible scenarios:

    - uninitialized data (usually initialized with zeroes)
    - writeable data
    - read-only data

  Depending on which scenario we will do one of the following:

    - uninitialized: uint8_t data[SIZE];
    - writable     : uint8_t *data = {0x12, ... };
    - read-only    : const uint8_t *data = {0x12, ... };
  """

  # first, ignore sections that have no value
  if section.type == 'SHT_NULL':
    return None

  comment = f'// { section.name } \n'

  # TODO: section_INDEX should be changed and/or section should have a proper
  #       public name
  name = f'section_{section.index}'
  if section.isBss():
    if section.hasWriteFlag():
      ctype = 'uint8_t'

    else: #read-only
      ctype = 'const uint8_t'

    return A64Block(
      A64Block.TYPE_DATA,
      name,
      comment +
      f"{ctype} {name} [{section.size}];"
    );

  elif section.isData():
    if section.hasWriteFlag():
      ctype = 'uint8_t'
    else: #read-only
      ctype = 'const uint8_t'

    hexbytes = []
    for offset in range(0, len(section.data), 16):
      hexbytes.append (','.join(['0x%02x' % c for c in section.data[offset:offset+16]]))

    hexbytes = ',\n  '.join(hexbytes);
    return A64Block(
      A64Block.TYPE_DATA,
      name,
      comment +
      f"{ctype} {name} [{section.size}] = {{\n  {hexbytes}\n}};"
    );

  # Everything else is just ignored (for now)
  return A64Block(
    A64Block.TYPE_IGNORED,
    name,
    '// ' + '\n// '.join (repr(section).split('\n'))
  );

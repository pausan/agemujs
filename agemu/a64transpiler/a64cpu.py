#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
import capstone
import json
import yaml
import random

class A64Cpu:
  """ Models a CPU register state and has some functions to display and
  manipulate. Mostly useful to compare with expected results when running
  test programs.
  """
  def __init__(self):
    self.reset()

  def reset (self):
    """ Create a CPU state
    """
    self.x0 = 0
    self.x1 = 0
    self.x2 = 0
    self.x3 = 0
    self.x4 = 0
    self.x5 = 0
    self.x6 = 0
    self.x7 = 0
    self.x8 = 0
    self.x9 = 0
    self.x10 = 0
    self.x11 = 0
    self.x12 = 0
    self.x13 = 0
    self.x14 = 0
    self.x15 = 0
    self.x16 = 0
    self.x17 = 0
    self.x18 = 0
    self.x19 = 0
    self.x20 = 0
    self.x21 = 0
    self.x22 = 0
    self.x23 = 0
    self.x24 = 0
    self.x25 = 0
    self.x26 = 0
    self.x27 = 0
    self.x28 = 0
    self.x29 = 0
    self.x30 = 0

    self.xzr = 0
    self.sp = 0

    self.pstate_overflow = False
    self.pstate_zero     = False
    self.pstate_carry    = False
    self.pstate_negative = False

    return self

  def w(self, xreg):
    """ Get the W part of given register (lower 32-bits)
    """
    return (xreg & 0xFFFF_FFFF)

  def get(self, key, default = None):
    return self.__dict__.get(key, default)

  def setValues(self, valuesDict):
    """ Set given set of register values from a dictionary of key values
    where keys should match keys in the current registers object. Registers
    specified in the input valuesDict which are not part of this object
    will be ignored
    """
    for key, value in valuesDict.items():
      if key in self.__dict__:
        self.__dict__[key] = valuesDict[key]

      elif key.replace('w', 'x') in self.__dict__:
        self.__dict__[key.replace('w', 'x')] = valuesDict[key]

      else:
        print ("WARNING: setting unknown CPU state key: ", key)
        pass # just ignore it!

    return

  def randomize(self):
    """ Randomize all registers and flags except SP and ZR.
    Useful for tests.
    """
    for key, value in self.__dict__.items():
      if key.startswith ('pstate_'):
        self.__dict__[key] = True if random.randint(0, 1) == 0 else False
      elif key.startswith('x'):
        self.__dict__[key] = random.randint(0, 2**64-1)

  def toDict(self):
    return self.__dict__

  def repr(self):
    return json.dumps(self.toDict(), indent = 2)

class A64Register:
  def __init__(self, name, size):
    assert (size == 32 or size == 64)
    self.name = name
    self.size = size

  def is32bit(self):
    return self.size == 32

  def is64bit(self):
    return self.size == 64

  @staticmethod
  def lookup(csRegId):
    """ Lookup a capstone register
    """
    return ARM64_REGISTERS.get (csRegId, None)


def loadCpuStateDict (text):
  """ Loads a JSON-based CPU state string.
  YAML loader is used instead of JSON one because of 64-bit hexadecimal
  numbers.
  """
  return yaml.load(text, Loader=yaml.FullLoader)

ARM64_W_TO_X = {
  capstone.arm64.ARM64_REG_W0 : capstone.arm64.ARM64_REG_X0,
  capstone.arm64.ARM64_REG_W1 : capstone.arm64.ARM64_REG_X1,
  capstone.arm64.ARM64_REG_W2 : capstone.arm64.ARM64_REG_X2,
  capstone.arm64.ARM64_REG_W3 : capstone.arm64.ARM64_REG_X3,
  capstone.arm64.ARM64_REG_W4 : capstone.arm64.ARM64_REG_X4,
  capstone.arm64.ARM64_REG_W5 : capstone.arm64.ARM64_REG_X5,
  capstone.arm64.ARM64_REG_W6 : capstone.arm64.ARM64_REG_X6,
  capstone.arm64.ARM64_REG_W7 : capstone.arm64.ARM64_REG_X7,
  capstone.arm64.ARM64_REG_W8 : capstone.arm64.ARM64_REG_X8,
  capstone.arm64.ARM64_REG_W9 : capstone.arm64.ARM64_REG_X9,
  capstone.arm64.ARM64_REG_W10 : capstone.arm64.ARM64_REG_X10,
  capstone.arm64.ARM64_REG_W11 : capstone.arm64.ARM64_REG_X11,
  capstone.arm64.ARM64_REG_W12 : capstone.arm64.ARM64_REG_X12,
  capstone.arm64.ARM64_REG_W13 : capstone.arm64.ARM64_REG_X13,
  capstone.arm64.ARM64_REG_W14 : capstone.arm64.ARM64_REG_X14,
  capstone.arm64.ARM64_REG_W15 : capstone.arm64.ARM64_REG_X15,
  capstone.arm64.ARM64_REG_W16 : capstone.arm64.ARM64_REG_X16,
  capstone.arm64.ARM64_REG_W17 : capstone.arm64.ARM64_REG_X17,
  capstone.arm64.ARM64_REG_W18 : capstone.arm64.ARM64_REG_X18,
  capstone.arm64.ARM64_REG_W19 : capstone.arm64.ARM64_REG_X19,
  capstone.arm64.ARM64_REG_W20 : capstone.arm64.ARM64_REG_X20,
  capstone.arm64.ARM64_REG_W21 : capstone.arm64.ARM64_REG_X21,
  capstone.arm64.ARM64_REG_W22 : capstone.arm64.ARM64_REG_X22,
  capstone.arm64.ARM64_REG_W23 : capstone.arm64.ARM64_REG_X23,
  capstone.arm64.ARM64_REG_W24 : capstone.arm64.ARM64_REG_X24,
  capstone.arm64.ARM64_REG_W25 : capstone.arm64.ARM64_REG_X25,
  capstone.arm64.ARM64_REG_W26 : capstone.arm64.ARM64_REG_X26,
  capstone.arm64.ARM64_REG_W27 : capstone.arm64.ARM64_REG_X27,
  capstone.arm64.ARM64_REG_W28 : capstone.arm64.ARM64_REG_X28,
  capstone.arm64.ARM64_REG_W29 : capstone.arm64.ARM64_REG_X29,
  capstone.arm64.ARM64_REG_W30 : capstone.arm64.ARM64_REG_X30,

  capstone.arm64.ARM64_REG_WZR : capstone.arm64.ARM64_REG_XZR,
  capstone.arm64.ARM64_REG_WSP : capstone.arm64.ARM64_REG_SP
}

ARM64_W32BIT_REG = set(ARM64_W_TO_X.keys())
ARM64_X64BIT_REG = set(ARM64_W_TO_X.values())

ARM64_REGISTERS = {
  capstone.arm64.ARM64_REG_W0 : A64Register('x0', 32),
  capstone.arm64.ARM64_REG_W1 : A64Register('x1', 32),
  capstone.arm64.ARM64_REG_W2 : A64Register('x2', 32),
  capstone.arm64.ARM64_REG_W3 : A64Register('x3', 32),
  capstone.arm64.ARM64_REG_W4 : A64Register('x4', 32),
  capstone.arm64.ARM64_REG_W5 : A64Register('x5', 32),
  capstone.arm64.ARM64_REG_W6 : A64Register('x6', 32),
  capstone.arm64.ARM64_REG_W7 : A64Register('x7', 32),
  capstone.arm64.ARM64_REG_W8 : A64Register('x8', 32),
  capstone.arm64.ARM64_REG_W9 : A64Register('x9', 32),
  capstone.arm64.ARM64_REG_W10 : A64Register('x10', 32),
  capstone.arm64.ARM64_REG_W11 : A64Register('x11', 32),
  capstone.arm64.ARM64_REG_W12 : A64Register('x12', 32),
  capstone.arm64.ARM64_REG_W13 : A64Register('x13', 32),
  capstone.arm64.ARM64_REG_W14 : A64Register('x14', 32),
  capstone.arm64.ARM64_REG_W15 : A64Register('x15', 32),
  capstone.arm64.ARM64_REG_W16 : A64Register('x16', 32),
  capstone.arm64.ARM64_REG_W17 : A64Register('x17', 32),
  capstone.arm64.ARM64_REG_W18 : A64Register('x18', 32),
  capstone.arm64.ARM64_REG_W19 : A64Register('x19', 32),
  capstone.arm64.ARM64_REG_W20 : A64Register('x20', 32),
  capstone.arm64.ARM64_REG_W21 : A64Register('x21', 32),
  capstone.arm64.ARM64_REG_W22 : A64Register('x22', 32),
  capstone.arm64.ARM64_REG_W23 : A64Register('x23', 32),
  capstone.arm64.ARM64_REG_W24 : A64Register('x24', 32),
  capstone.arm64.ARM64_REG_W25 : A64Register('x25', 32),
  capstone.arm64.ARM64_REG_W26 : A64Register('x26', 32),
  capstone.arm64.ARM64_REG_W27 : A64Register('x27', 32),
  capstone.arm64.ARM64_REG_W28 : A64Register('x28', 32),
  capstone.arm64.ARM64_REG_W29 : A64Register('x29', 32),
  capstone.arm64.ARM64_REG_W30 : A64Register('x30', 32),
  capstone.arm64.ARM64_REG_WZR : A64Register('xzr', 32),

  capstone.arm64.ARM64_REG_X0 : A64Register('x0', 64),
  capstone.arm64.ARM64_REG_X1 : A64Register('x1', 64),
  capstone.arm64.ARM64_REG_X2 : A64Register('x2', 64),
  capstone.arm64.ARM64_REG_X3 : A64Register('x3', 64),
  capstone.arm64.ARM64_REG_X4 : A64Register('x4', 64),
  capstone.arm64.ARM64_REG_X5 : A64Register('x5', 64),
  capstone.arm64.ARM64_REG_X6 : A64Register('x6', 64),
  capstone.arm64.ARM64_REG_X7 : A64Register('x7', 64),
  capstone.arm64.ARM64_REG_X8 : A64Register('x8', 64),
  capstone.arm64.ARM64_REG_X9 : A64Register('x9', 64),
  capstone.arm64.ARM64_REG_X10 : A64Register('x10', 64),
  capstone.arm64.ARM64_REG_X11 : A64Register('x11', 64),
  capstone.arm64.ARM64_REG_X12 : A64Register('x12', 64),
  capstone.arm64.ARM64_REG_X13 : A64Register('x13', 64),
  capstone.arm64.ARM64_REG_X14 : A64Register('x14', 64),
  capstone.arm64.ARM64_REG_X15 : A64Register('x15', 64),
  capstone.arm64.ARM64_REG_X16 : A64Register('x16', 64),
  capstone.arm64.ARM64_REG_X17 : A64Register('x17', 64),
  capstone.arm64.ARM64_REG_X18 : A64Register('x18', 64),
  capstone.arm64.ARM64_REG_X19 : A64Register('x19', 64),
  capstone.arm64.ARM64_REG_X20 : A64Register('x20', 64),
  capstone.arm64.ARM64_REG_X21 : A64Register('x21', 64),
  capstone.arm64.ARM64_REG_X22 : A64Register('x22', 64),
  capstone.arm64.ARM64_REG_X23 : A64Register('x23', 64),
  capstone.arm64.ARM64_REG_X24 : A64Register('x24', 64),
  capstone.arm64.ARM64_REG_X25 : A64Register('x25', 64),
  capstone.arm64.ARM64_REG_X26 : A64Register('x26', 64),
  capstone.arm64.ARM64_REG_X27 : A64Register('x27', 64),
  capstone.arm64.ARM64_REG_X28 : A64Register('x28', 64),
  capstone.arm64.ARM64_REG_X29 : A64Register('x29', 64),
  capstone.arm64.ARM64_REG_X30 : A64Register('x30', 64),
  capstone.arm64.ARM64_REG_XZR : A64Register('xzr', 64),

  capstone.arm64.ARM64_REG_WSP : A64Register('sp', 32),
  capstone.arm64.ARM64_REG_SP  : A64Register('sp', 64),
}

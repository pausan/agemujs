// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH Load-like instructions
// -----------------------------------------------------------------------------
#include "core/a64cpu.h"
#include "core/a64util.h"

// -----------------------------------------------------------------------------
// a64_ldr_*
// -----------------------------------------------------------------------------

// ldr Xt, [Xn]
//
// e.g: ldr x0, [sp]
FORCE_INLINE
void a64_ldr_xm (
  a64cpu_t *ctx,
  a64reg_t *Xt,
  a64reg_t *Xn
) {
  *Xt = A64_MEM_VALUE64(*Xn, 0);
}

// ldr Wt, [Xn]
//
// e.g: ldr x0, [sp]
FORCE_INLINE
void a64_ldr_wm (
  a64cpu_t *ctx,
  a64reg_t *Wt,
  a64reg_t *Xn
) {
  *Wt = A64_MEM_VALUE32(*Xn, 0);
}

// ldr Xt, [Xn, #pimm]
//
// e.g: ldr x0, [sp, #8]
FORCE_INLINE
void a64_ldr_xm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt,
  a64reg_t *Xn,
  const a64pimm_t pimm
) {
  *Xt = A64_MEM_VALUE64(*Xn, pimm);
}

// ldr Wt, [Xn, #pimm]
//
// e.g: ldr x0, [sp, #8]
FORCE_INLINE
void a64_ldr_wm_off (
  a64cpu_t *ctx,
  a64reg_t *Wt,
  a64reg_t *Xn,
  const a64pimm_t pimm
) {
  *Wt = A64_MEM_VALUE64(*Xn, pimm);
}

// -----------------------------------------------------------------------------
// a64_ldp_*
// -----------------------------------------------------------------------------
// e.g:   ldp x29, x30, [sp]
FORCE_INLINE
void a64_ldp_xxm (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem
) {
  *Xt1 = A64_MEM_VALUE64(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE64(*Xmem, 8);
}

// e.g:   ldp w29, w30, [sp]
FORCE_INLINE
void a64_ldp_wwm (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem
) {
  // set w with high 32-bits to 0
  *Xt1 = A64_MEM_VALUE32(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE32(*Xmem, 4);
}

// e.g:   ldp x29, x30, [sp, -16]
FORCE_INLINE
void a64_ldp_xxm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xt1 = A64_MEM_VALUE64(*Xmem, disp + 0);
  *Xt2 = A64_MEM_VALUE64(*Xmem, disp + 8);
}

// e.g:   ldp wzr, w30, [sp, -16]
FORCE_INLINE
void a64_ldp_wwm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xt1 = A64_MEM_VALUE32(*Xmem, disp + 0);
  *Xt2 = A64_MEM_VALUE32(*Xmem, disp + 4);
}

// e.g:   ldp x29, x30, [sp, -16]!
FORCE_INLINE
void a64_ldp_xxm_pre (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xmem += disp;
  *Xt1 = A64_MEM_VALUE64(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE64(*Xmem, 8);
}

// e.g:   ldp w29, w30, [sp, -16]!
FORCE_INLINE
void a64_ldp_wwm_pre (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xmem += disp;
  *Xt1 = A64_MEM_VALUE32(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE32(*Xmem, 4);
}

// e.g:   ldp x29, x30, [sp], -16
FORCE_INLINE
void a64_ldp_xxm_post (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xt1 = A64_MEM_VALUE64(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE64(*Xmem, 8);
  *Xmem += disp;
}

// e.g:   ldp w29, w30, [sp], -16
FORCE_INLINE
void a64_ldp_wwm_post (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xt1 = A64_MEM_VALUE32(*Xmem, 0);
  *Xt2 = A64_MEM_VALUE32(*Xmem, 4);
  *Xmem += disp;
}

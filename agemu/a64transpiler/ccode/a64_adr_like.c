// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH PC Address generation instructions
// -----------------------------------------------------------------------------
#include "core/a64cpu.h"
#include "core/a64util.h"

// TODO:
// // ADRP Xd, LABEL
// FORCE_INLINE
// void a64_adrp (a64cpu_t *ctx, a64reg_t *rdst) {
//   rdst->x = rsrc->x;
// }
// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH Move instructions
// -----------------------------------------------------------------------------
#include "core/a64cpu.h"
#include "core/a64util.h"

// MOV Xd|SP, Xn|SP
FORCE_INLINE
void a64_mov_xx (a64cpu_t *ctx, a64reg_t *Xd, const a64reg_t *Xm) {
  *Xd = *Xm;
}

// MOV Wd|WSP, Wn|WSP
FORCE_INLINE
void a64_mov_ww (a64cpu_t *ctx, a64reg_t *Xd, const a64reg_t *Xm) {
  *Xd = A64_LOW32(*Xm);
}

FORCE_INLINE
void a64_movz_xi (a64cpu_t *ctx, a64reg_t *Xd, a64imm_t imm) {
  *Xd = imm;
}

FORCE_INLINE
void a64_movz_wi (a64cpu_t *ctx, a64reg_t *Xd, a64imm_t imm) {
  *Xd = A64_LOW32(imm);
}

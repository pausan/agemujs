// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH CPU Instructions
//
// System-level registers and instructions are out of the scope of this project
// and won't be modeled.
// -----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "core/a64cpu.h"
#include "core/a64util.h"
#include "core/a64util.c"

#include "a64_add_like.c"
#include "a64_adr_like.c"
#include "a64_load_like.c"
#include "a64_mov_like.c"
#include "a64_nop.c"
#include "a64_ret.c"
#include "a64_store_like.c"

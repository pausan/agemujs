// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH PC Address generation instructions
// -----------------------------------------------------------------------------
#include "core/a64cpu.h"
#include "core/a64util.h"

// Reference:
// https://developer.arm.com/architectures/learn-the-architecture/armv8-a-instruction-set-architecture/data-processing-arithmetic-and-logic-operations

// e.g: add Xd, Xn, Xm
//
// No flags are modified
FORCE_INLINE
void a64_add_xxx (
  a64cpu_t *ctx,
  a64reg_t *Rd,
  a64reg_t *Xn,
  a64reg_t *Xm
) {
  *Rd = *Xn + *Xm;
}

// e.g: add Wd, Wn, Wm
//
// No flags are modified
FORCE_INLINE
void a64_add_www (
  a64cpu_t *ctx,
  a64reg_t *Wd,
  a64reg_t *Wn,
  a64reg_t *Wm
) {
  *Wd = A64_LOW32(A64_LOW32(*Wn) + A64_LOW32(*Wm));
}

// e.g: add Xd, Xn, imm
//
// No flags are modified
FORCE_INLINE
void a64_add_xxi (
  a64cpu_t *ctx,
  a64reg_t *Xd,
  a64reg_t *Xn,
  a64imm_t imm
) {
  *Xd = *Xn + imm;
}

// e.g: add Wd, Wn, imm
//
// No flags are modified
FORCE_INLINE
void a64_add_wwi (
  a64cpu_t *ctx,
  a64reg_t *Wd,
  a64reg_t *Wn,
  a64imm_t imm
) {
  *Wd = A64_LOW32(A64_LOW32(*Wn) + A64_LOW32(imm));
}

// e.g: sub Rd, Xn, imm
//
// No flags are modified
FORCE_INLINE
void a64_sub_xxi (
  a64cpu_t *ctx,
  a64reg_t *Rd,
  a64reg_t *Xn,
  a64imm_t imm
) {
  *Rd = *Xn - imm;
}


// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH Store-like instructions
// -----------------------------------------------------------------------------
#include "core/a64cpu.h"
#include "core/a64util.h"

// -----------------------------------------------------------------------------
// a64_str_*
// -----------------------------------------------------------------------------

// str Xt, [Xn]
//
// e.g: str x0, [sp]
FORCE_INLINE
void a64_str_xm (
  a64cpu_t *ctx,
  a64reg_t *Xt,
  a64reg_t *Xn
) {
  A64_MEM_VALUE64(*Xn, 0) = *Xt;
}

// str Wt, [Xn]
//
// e.g: str w0, [sp]
FORCE_INLINE
void a64_str_wm (
  a64cpu_t *ctx,
  a64reg_t *Wt,
  a64reg_t *Xn
) {
  A64_MEM_VALUE32(*Xn, 0) = *Wt;
}

// str Xt, [Xn, #pimm]
//
// e.g: str x0, [sp, #8]
FORCE_INLINE
void a64_str_xm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt,
  a64reg_t *Xn,
  const a64pimm_t pimm
) {
  A64_MEM_VALUE64(*Xn, pimm) = *Xt;
}

// str Wt, [Xn, #pimm]
//
// e.g: str w0, [sp, #8]
FORCE_INLINE
void a64_str_wm_off (
  a64cpu_t *ctx,
  a64reg_t *Wt,
  a64reg_t *Xn,
  const a64pimm_t pimm
) {
  A64_MEM_VALUE32(*Xn, pimm) = *Wt;
}

// -----------------------------------------------------------------------------
// a64_stp_*
// -----------------------------------------------------------------------------
// e.g:   stp x29, x30, [sp]
FORCE_INLINE
void a64_stp_xxm (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem
) {
  A64_MEM_VALUE64(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE64(*Xmem, 8) = *Xt2;
}

// e.g:   stp w29, w30, [sp]
FORCE_INLINE
void a64_stp_wwm (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem
) {
  // set w with high 32-bits to 0
  A64_MEM_VALUE32(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE32(*Xmem, 4) = *Xt2;
}

// e.g:   stp x29, x30, [sp, -16]
FORCE_INLINE
void a64_stp_xxm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem,
  a64imm_t disp
) {
  A64_MEM_VALUE64(*Xmem, disp + 0) = *Xt1;
  A64_MEM_VALUE64(*Xmem, disp + 8) = *Xt2;
}

// e.g:   stp wzr, w30, [sp, -16]
FORCE_INLINE
void a64_stp_wwm_off (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  const a64reg_t *Xmem,
  a64imm_t disp
) {
  A64_MEM_VALUE32(*Xmem, disp + 0) = *Xt1;
  A64_MEM_VALUE32(*Xmem, disp + 4) = *Xt2;
}

// e.g:   stp x29, x30, [sp, -16]!
FORCE_INLINE
void a64_stp_xxm_pre (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xmem += disp;
  A64_MEM_VALUE64(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE64(*Xmem, 8) = *Xt2;
}

// e.g:   stp w29, w30, [sp, -16]!
FORCE_INLINE
void a64_stp_wwm_pre (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  *Xmem += disp;
  A64_MEM_VALUE32(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE32(*Xmem, 4) = *Xt2;
}

// e.g:   stp x29, x30, [sp], -16
FORCE_INLINE
void a64_stp_xxm_post (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  A64_MEM_VALUE64(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE64(*Xmem, 8) = *Xt2;
  *Xmem += disp;
}

// e.g:   stp w29, w30, [sp], -16
FORCE_INLINE
void a64_stp_wwm_post (
  a64cpu_t *ctx,
  a64reg_t *Xt1,
  a64reg_t *Xt2,
  a64reg_t *Xmem,
  a64imm_t disp
) {
  A64_MEM_VALUE32(*Xmem, 0) = *Xt1;
  A64_MEM_VALUE32(*Xmem, 4) = *Xt2;
  *Xmem += disp;
}

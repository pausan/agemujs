// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH64 CPU & Registers
//
// System-level registers and instructions are out of the scope of this project
// and won't be modeled.
//
// a64reg_t
//
//   AARCH64 Register with a 64-bit part 'x' and two 32-bit parts. Low 32-bits
//   is referenced as 'w' and high 32-bit is referenced as 'wh'.
//
// a64cpu_t
//
//   Contains all AARCH64 CPU registers and flags.
// -----------------------------------------------------------------------------
#ifndef __agemu_a64_transpiler__a64cpu_h__
#define __agemu_a64_transpiler__a64cpu_h__

#include "types.h"

#define FLAGOP_IGNORE_FLAGS 0
#define FLAGOP_UPDATE_FLAGS 1
typedef uint_fast8_t flagop;

#pragma pack(push, 1)

typedef uint32_t a64imm_t;
typedef uint32_t a64pimm_t;
typedef int32_t  a64simm_t;

// Most operations that access to W register modify the upper part,
// so it is easier to deal with a single X register instead of a complex
// union
typedef uint64_t  a64reg_t;

// 4.5. Changing execution state (again)
//
// In general, application programmers write applications for either AArch32
// or AArch64. It is only the OS that must take account of the two execution
// states and the switch between them.
//
// ---
// Registers in AARCH64 have 'x' or 'w' prefix. Here we opted to represent
// 'x' but to be clear we will use 'wreg' macro for the lower part
//
typedef struct {
  // yes, an array could have been created, but this is more convenient to
  // read generated code
  a64reg_t  x0;
  a64reg_t  x1;
  a64reg_t  x2;
  a64reg_t  x3;
  a64reg_t  x4;
  a64reg_t  x5;
  a64reg_t  x6;
  a64reg_t  x7;
  a64reg_t  x8;
  a64reg_t  x9;
  a64reg_t  x10;
  a64reg_t  x11;
  a64reg_t  x12;
  a64reg_t  x13;
  a64reg_t  x14;
  a64reg_t  x15;
  a64reg_t  x16;
  a64reg_t  x17;
  a64reg_t  x18;
  a64reg_t  x19;
  a64reg_t  x20;
  a64reg_t  x21;
  a64reg_t  x22;
  a64reg_t  x23;
  a64reg_t  x24;
  a64reg_t  x25;
  a64reg_t  x26;
  a64reg_t  x27;
  a64reg_t  x28;
  a64reg_t  x29; // frame pointer
  a64reg_t  x30; // procedure link register

  // r31 = zero register, usually used to store zero and reduce number of
  // operations needed, but can have any value
  a64reg_t  xzr;

  uint8_t  *stack_base; // internal: base allocation pointer for the stack
  uint32_t  stack_size; // internal: stack max size
  a64reg_t  sp;  // usually sp and wsp, now sp should be references as sp.x

  uint64_t  pc;  // implicit

  uint64_t  elr;  // Exception Link Register: exception return address. depends on flag_m03
  uint64_t  spsr; // Saved Program Status Register: depends on flag_m03
                  // holds current state flags to be restored after the exception

  // Splitting pstate flag into several variables for convenience
  uint_fast8_t pstate_overflow; // v flag
  uint_fast8_t pstate_zero;     // z flag
  uint_fast8_t pstate_carry;    // c flag
  uint_fast8_t pstate_negative; // n flag

  // uint_fast8_t pstate_step;     // ss flag / software step
  // uint_fast8_t pstate_illegal;  // il flag / illegal execution state bit
  // uint_fast8_t pstate_debug;    // d flag / process state debug flag
  // uint_fast8_t pstate_system;   // a flag / system error
  // uint_fast8_t pstate_irq;      // irq flag
  // uint_fast8_t pstate_fiq;      // fiq flag

  //  uint_fast8_t pstate_m4;       // m[4] flag, 0 = exception in execution state of aarch64
  //  uint_fast8_t pstate_m03;      // m[0:3] flag mode/exception level
} a64cpu_t;

#pragma pack(pop)

#endif // __agemu_a64_transpiler__a64cpu_h__

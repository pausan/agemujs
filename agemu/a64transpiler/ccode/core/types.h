// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
#ifndef __agemu_a64_transpiler__types_h__
#define __agemu_a64_transpiler__types_h__

#include <stdint.h>
#define FORCE_INLINE __attribute__((always_inline)) inline

#endif // __agemu_a64_transpiler__types_h__

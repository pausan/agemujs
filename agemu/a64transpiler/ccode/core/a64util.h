// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH64 CPU Util function declarations
// -----------------------------------------------------------------------------
#ifndef __agemu_a64_transpiler__a64util_h__
#define __agemu_a64_transpiler__a64util_h__

#include "a64cpu.h"

// Returns the 64-bit or 32-bit associated to given memory address
#define A64_MEM_DISP(addr, disp)      (((uint8_t*)(addr)) + disp)
#define A64_MEM_VALUE64(addr, disp)   (*((uint64_t*)(A64_MEM_DISP(addr, disp))))
#define A64_MEM_VALUE32(addr, disp)   (*((uint32_t*)(A64_MEM_DISP(addr, disp))))

#define A64_LOW32(X)  ((X) & 0xFFFFFFFF)

typedef enum  {
  OUTPUT_FORMAT_JSON         = 0x00,
  OUTPUT_FORMAT_JSON_COMPACT = 0x01,
} output_format_t;

void a64_internal_show_cpu_state (
  const a64cpu_t *cpu,
  output_format_t format
);

#endif // __agemu_a64_transpiler__a64util_h__

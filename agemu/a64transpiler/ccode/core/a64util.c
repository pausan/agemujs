// -----------------------------------------------------------------------------
// Copyright (c) 2019 Pau Sanchez - MIT License
// -----------------------------------------------------------------------------
// AARCH64 CPU Util functions
// -----------------------------------------------------------------------------
#include <inttypes.h>
#include "a64util.h"

// -----------------------------------------------------------------------------
// _a64_internal_show_register_json
// -----------------------------------------------------------------------------
static void _a64_internal_show_register64_json (
  const char *name,
  uint64_t value,
  output_format_t format
)
{
  char qname[64];
  char newline = (format == OUTPUT_FORMAT_JSON_COMPACT) ? ' ' : '\n';

  if (format == OUTPUT_FORMAT_JSON_COMPACT) {
    if (value == 0)
      printf  ("\"%s\" : 0,", name);
    else
      printf  ("\"%s\" : 0x%" PRIx64 ",", name, value);
  }
  else {
    snprintf(qname, 64, "\"%s\"", name);
    printf  ("%7s : 0x%08x_%08x,", qname, (uint32_t)(value>>32), (uint32_t)(value));
  }

  putchar (newline);
}

// -----------------------------------------------------------------------------
// _a64_internal_show_register_json
// -----------------------------------------------------------------------------
static void _a64_internal_show_register_json (
  const char *name,
  a64reg_t reg,
  output_format_t format
) {
  char xname[64];

  // prepend x to the register name passed
  snprintf(xname, 64, "x%s", name);
  _a64_internal_show_register64_json (xname, reg, format);
}

// -----------------------------------------------------------------------------
// _a64_internal_show_flag_state_json
// -----------------------------------------------------------------------------
static void _a64_internal_show_flag_state_json (const a64cpu_t *cpu, output_format_t format) {
  char *newline = (format == OUTPUT_FORMAT_JSON_COMPACT) ? "" : "\n  ";

  printf ("%s", newline);
  printf ("\"pstate_overflow\" : %s,", cpu->pstate_overflow ? "true" : "false");
  printf ("%s", newline);
  printf ("\"pstate_zero\"     : %s,", cpu->pstate_zero     ? "true" : "false");
  printf ("%s", newline);
  printf ("\"pstate_carry\"    : %s,", cpu->pstate_carry    ? "true" : "false");
  printf ("%s", newline);
  printf ("\"pstate_negative\" : %s",  cpu->pstate_negative ? "true" : "false");

  if (format != OUTPUT_FORMAT_JSON_COMPACT)
    printf ("\n");
}

// -----------------------------------------------------------------------------
// a64_internal_show_cpu_state
//
// Shows CPU State in JSON format, so it is both human and machine readable
// -----------------------------------------------------------------------------
void a64_internal_show_cpu_state (const a64cpu_t *cpu, output_format_t format) {
  char newline = (format == OUTPUT_FORMAT_JSON_COMPACT) ? ' ' : '\n';
  putchar ('{');
  putchar (newline);

  _a64_internal_show_register_json ("0", cpu->x0, format);
  _a64_internal_show_register_json ("1", cpu->x1, format);
  _a64_internal_show_register_json ("2", cpu->x2, format);
  _a64_internal_show_register_json ("3", cpu->x3, format);
  _a64_internal_show_register_json ("4", cpu->x4, format);
  _a64_internal_show_register_json ("5", cpu->x5, format);
  _a64_internal_show_register_json ("6", cpu->x6, format);
  _a64_internal_show_register_json ("7", cpu->x7, format);
  _a64_internal_show_register_json ("8", cpu->x8, format);
  _a64_internal_show_register_json ("9", cpu->x9, format);
  _a64_internal_show_register_json ("10", cpu->x10, format);
  _a64_internal_show_register_json ("11", cpu->x11, format);
  _a64_internal_show_register_json ("12", cpu->x12, format);
  _a64_internal_show_register_json ("13", cpu->x13, format);
  _a64_internal_show_register_json ("14", cpu->x14, format);
  _a64_internal_show_register_json ("15", cpu->x15, format);
  _a64_internal_show_register_json ("16", cpu->x16, format);
  _a64_internal_show_register_json ("17", cpu->x17, format);
  _a64_internal_show_register_json ("18", cpu->x18, format);
  _a64_internal_show_register_json ("19", cpu->x19, format);
  _a64_internal_show_register_json ("20", cpu->x20, format);
  _a64_internal_show_register_json ("21", cpu->x21, format);
  _a64_internal_show_register_json ("22", cpu->x22, format);
  _a64_internal_show_register_json ("23", cpu->x23, format);
  _a64_internal_show_register_json ("24", cpu->x24, format);
  _a64_internal_show_register_json ("25", cpu->x25, format);
  _a64_internal_show_register_json ("26", cpu->x26, format);
  _a64_internal_show_register_json ("27", cpu->x27, format);
  _a64_internal_show_register_json ("28", cpu->x28, format);
  _a64_internal_show_register_json ("29", cpu->x29, format);
  _a64_internal_show_register_json ("30", cpu->x30, format);
  _a64_internal_show_register_json ("zr", cpu->xzr, format);

  _a64_internal_show_register64_json ("sp", cpu->sp, format);

  _a64_internal_show_register64_json ("stack_base", (uint64_t)cpu->stack_base, format);
  _a64_internal_show_register64_json ("stack_size", (uint64_t)cpu->stack_size, format);

  _a64_internal_show_flag_state_json (cpu, format);

  putchar ('}');
  putchar ('\n');
}

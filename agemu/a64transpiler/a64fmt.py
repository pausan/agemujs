#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
# Concerned of formatting C code
#
from . import a64cpu, a64util

from capstone import arm64 as csarm64
from elftools.elf import enums as elfenums

def hexint(value):
  """ Format an hex integer (positive or negative values).
  """
  if value < 0:
    return f'-0x{-value:08x}'
  return f'0x{value:08x}'

def address(addr, relocs = None):
  """ Formats an address by specifying an absolute address plus any
  math regarding allocations
  """
  addrSum = [hexint(addr)]

  if relocs:
    assert (len(relocs) == 1)
    reloc = relocs[0]

    # higher 21 bits
    if (reloc.type == elfenums.ENUM_RELOC_TYPE_AARCH64['R_AARCH64_ADR_PREL_PG_HI21']):
      addrSum = [hexint (addr + reloc.symbol.addr + reloc.addend)]
      addrSum.append ('(((uint64_t)section_' + str(reloc.symbol.sectionOwner.index) + ') >> 21)')

    # lower 12 bits
    elif (reloc.type == elfenums.ENUM_RELOC_TYPE_AARCH64['R_AARCH64_ADD_ABS_LO12_NC']):
      addrSum = [hexint (addr + reloc.symbol.addr + reloc.addend)]
      addrSum.append ('(((uint64_t)section_' + str(reloc.symbol.sectionOwner.index) + ') & 0x0FFF)')

    # lower 26 bits (can address 64MB)
    elif (reloc.type == elfenums.ENUM_RELOC_TYPE_AARCH64['R_AARCH64_CALL26']):
      addrSum = [hexint (addr + reloc.symbol.addr + reloc.addend)]
      addrSum.append ('(((uint64_t)section_' + str(reloc.symbol.sectionOwner.index) + ') & 0x03FFFFFF)')

    else:
      refName = None
      for name, type in elfenums.ENUM_RELOC_TYPE_AARCH64.items():
        if reloc.type == type:
          refName = name
          break
      raise Exception (f'Unsupported Addressing: {reloc.type} = {refName}')
      assert (False)

  return ' + '.join(addrSum)

def reg(regIndex):
  """ Formats a register

  Example:

    >>> regt(reg)
    '&(cpu->r3)'
  """
  r = a64cpu.A64Register.lookup(regIndex)
  return f'&(cpu->{r.name})'

def imm(imm, relocs = None):
  """ Immediate
  """
  if relocs:
    return address(imm, relocs)

  return hexint(imm)

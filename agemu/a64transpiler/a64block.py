#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#

class A64Block:
  """
  Example:

    >>> a64block = A64Block(
      A64Block.CODE,
      'my_func_name',
      ['a64_instr1(...);', ..., 'return;']
    );
  """
  TYPE_CODE = 'code'
  TYPE_DATA = 'data'
  TYPE_IGNORED = 'ignored'

  def __init__(self, type, name, ccode):
    assert (type in [
      A64Block.TYPE_CODE, A64Block.TYPE_DATA, A64Block.TYPE_IGNORED
    ])
    self.type = type
    self.name = name
    self.ccode = [ccode] if isinstance(ccode, str) else ccode

  def isCode(self):
    return self.type == A64Block.TYPE_CODE

  def isData(self):
    return self.type == A64Block.TYPE_DATA

  def isIgnored(self):
    return self.type == A64Block.TYPE_IGNORED

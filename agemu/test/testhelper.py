#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
import os
import sys
import json

import unicorn
from agemu import elflib

A64_UNICORN_REGMAP = {
  'x0' : unicorn.arm64_const.UC_ARM64_REG_X0,
  'x1' : unicorn.arm64_const.UC_ARM64_REG_X1,
  'x2' : unicorn.arm64_const.UC_ARM64_REG_X2,
  'x3' : unicorn.arm64_const.UC_ARM64_REG_X3,
  'x4' : unicorn.arm64_const.UC_ARM64_REG_X4,
  'x5' : unicorn.arm64_const.UC_ARM64_REG_X5,
  'x6' : unicorn.arm64_const.UC_ARM64_REG_X6,
  'x7' : unicorn.arm64_const.UC_ARM64_REG_X7,
  'x8' : unicorn.arm64_const.UC_ARM64_REG_X8,
  'x9' : unicorn.arm64_const.UC_ARM64_REG_X9,
  'x10' : unicorn.arm64_const.UC_ARM64_REG_X10,
  'x11' : unicorn.arm64_const.UC_ARM64_REG_X11,
  'x12' : unicorn.arm64_const.UC_ARM64_REG_X12,
  'x13' : unicorn.arm64_const.UC_ARM64_REG_X13,
  'x14' : unicorn.arm64_const.UC_ARM64_REG_X14,
  'x15' : unicorn.arm64_const.UC_ARM64_REG_X15,
  'x16' : unicorn.arm64_const.UC_ARM64_REG_X16,
  'x17' : unicorn.arm64_const.UC_ARM64_REG_X17,
  'x18' : unicorn.arm64_const.UC_ARM64_REG_X18,
  'x19' : unicorn.arm64_const.UC_ARM64_REG_X19,
  'x20' : unicorn.arm64_const.UC_ARM64_REG_X20,
  'x21' : unicorn.arm64_const.UC_ARM64_REG_X21,
  'x22' : unicorn.arm64_const.UC_ARM64_REG_X22,
  'x23' : unicorn.arm64_const.UC_ARM64_REG_X23,
  'x24' : unicorn.arm64_const.UC_ARM64_REG_X24,
  'x25' : unicorn.arm64_const.UC_ARM64_REG_X25,
  'x26' : unicorn.arm64_const.UC_ARM64_REG_X26,
  'x27' : unicorn.arm64_const.UC_ARM64_REG_X27,
  'x28' : unicorn.arm64_const.UC_ARM64_REG_X28,
  'x29' : unicorn.arm64_const.UC_ARM64_REG_X29,
  'x30' : unicorn.arm64_const.UC_ARM64_REG_X30,
  'xzr' : unicorn.arm64_const.UC_ARM64_REG_XZR,
  'sp'  : unicorn.arm64_const.UC_ARM64_REG_SP
}

A64_UNICORN_REGMAP_INV = {v: k for k, v in A64_UNICORN_REGMAP.items() }

# ARMv8 according to arm/cpu.h in unicorn/qemu/...
PSTATE_V = (1 << 28)
PSTATE_C = (1 << 29)
PSTATE_Z = (1 << 30)
PSTATE_N = (1 << 31)

def unicornSetAllRegisters(mu, regDict):
  nzcv = 0

  for regName, value in regDict.items():
    if regName in A64_UNICORN_REGMAP:
      mu.reg_write(A64_UNICORN_REGMAP[regName], value)

    elif regName == 'pstate_overflow':
      nzcv |= (PSTATE_V if value else 0)

    elif regName == 'pstate_zero':
      nzcv |= (PSTATE_Z if value else 0)

    elif regName == 'pstate_carry':
      nzcv |= (PSTATE_C if value else 0)

    elif regName == 'pstate_negative':
      nzcv |= (PSTATE_N if value else 0)

    else:
      # FIXME! set pstate_* flags
      print (f'ERROR: unicorn, unknown register: {regName}')

  mu.reg_write(unicorn.arm64_const.UC_ARM64_REG_NZCV, nzcv)

def unicornGetAllRegisters(mu):
  nzcv = mu.reg_read(unicorn.arm64_const.UC_ARM64_REG_NZCV)
  regs = {}
  for regId, regName in A64_UNICORN_REGMAP_INV.items():
    regs[regName] = mu.reg_read(regId)

  regs['pstate_overflow'] = ((nzcv & PSTATE_V) == PSTATE_V)
  regs['pstate_zero'] = ((nzcv & PSTATE_Z) == PSTATE_Z)
  regs['pstate_carry'] = ((nzcv & PSTATE_C) == PSTATE_C)
  regs['pstate_negative'] = ((nzcv & PSTATE_N) == PSTATE_N)

  return regs

def validateTestInUnicorn(
  fileName,
  testId,
  name,
  objectFile,
  inputs,
  outputs,
  **kwargs
):
  """ This method runs the test in Unicorn emulator in order to validate
  that the input/output registers are the ones expected. Useful to avoid
  human error when developing the tests themselves.
  """
  ADDRESS = 0x10000
  ARM64_CODE = None  # binary code to be executed

  obj = elflib.Object()
  obj.load (objectFile)
  obj.disassemble()

  # TODO: build a tiny linker for given object file
  # mem = obj.link(ADDRESS)

  blocks = []
  for section in obj.sections:
    #print ("section:", section)
    if not section.hasCode() or (len(section.instrs) == 0):
      continue

    if ARM64_CODE is None:
      ARM64_CODE = section.data
    else:
      raise Exception ("Only one testing code block supported for now")

  # mu = unicorn.Uc(UC_ARCH_ARM64, UC_MODE_ARM | unicorn.UC_MODE_BIG_ENDIAN)
  mu = unicorn.Uc(unicorn.UC_ARCH_ARM64, unicorn.UC_MODE_ARM)

  # map 4MB memory for this emulation
  mu.mem_map(ADDRESS, 4 * 1024 * 1024)
  mu.mem_write(ADDRESS, ARM64_CODE)

  unicornSetAllRegisters (mu, inputs)

  mu.emu_start(ADDRESS, ADDRESS + len(ARM64_CODE))

  cpuStateDict = unicornGetAllRegisters(mu)

  for regKey, regValue in outputs.items():
    realValue = cpuStateDict.get(regKey, None)
    if realValue != regValue:
      print (f'UNICORN WARNING: {regKey} expecting {regValue} but got {realValue}')

  return

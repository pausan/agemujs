// -----------------------------------------------------------------------------
// Simple test to check calling an external function
// -----------------------------------------------------------------------------
#include <unistd.h>
#include <stdio.h>

void say_hi(void) {
  // NOTE: the write call is not working when compiled with emscripten
  //       write(1, "Hi!\n", 4);
  puts ("hi!");
}

// -----------------------------------------------------------------------------
// Simple methods to test the most basic functionality
// -----------------------------------------------------------------------------
#include <stdint.h>

uint32_t return42_32bits(void) {
  return 42;
}

uint64_t return42_64bits(void) {
  return 42;
}

uint64_t sum_64bits(uint64_t a, uint64_t b) {
  return a+b;
}

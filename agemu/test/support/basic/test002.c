// -----------------------------------------------------------------------------
// Simple methods to test the most basic functionality
// -----------------------------------------------------------------------------
#include <stdint.h>

uint32_t return42_32bits(void) {
  return 42;
}

uint64_t return42_64bits(void) {
  return 42;
}

uint32_t sum42_32bits(uint32_t a) {
  return a + 42;
}

uint64_t sum42_64bits(uint64_t a) {
  return a + 42;
}

uint32_t sum_32bits(uint32_t a, uint32_t b) {
  return a + b;
}

uint64_t sum_64bits(uint64_t a, uint64_t b) {
  return a + b;
}

// -----------------------------------------------------------------------------
// Simple test to check if caller works as expected
// -----------------------------------------------------------------------------
#include <stdint.h>

uint64_t return42_64bits(void) {
  return 42;
}

uint32_t sum42_32bits(uint32_t a) {
  return a + 42;
}

uint64_t sum_64bits(uint64_t a, uint64_t b) {
  return a + b;
}

// (12+42) + 42 = 96
uint64_t call_test (void) {
  uint32_t x = sum42_32bits(12);
  return sum_64bits ((uint64_t)x, return42_64bits());
}
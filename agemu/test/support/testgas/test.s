# ------------------------------------------------------------------------------
# test.s
# Copyright (c) Pau Sanchez - MIT License
# ------------------------------------------------------------------------------
# The purpose of this file is to play with the instruction operands in order
# to ensure that the disassembler and transpiler work as expected.
# ------------------------------------------------------------------------------
.arch armv8-a
.file  "test.s"
.text

.section .rodata
.align  3

hi_test:
.string  "hi test!"

.section  .text,"ax",@progbits
.align  2
.global  test

test:
  stp  x29, x30, [sp]
  stp  w29, w30, [sp]

  stp  x29, x30, [sp, -16]
  stp  wzr, w30, [sp, -16]

  stp  x29, x30, [sp, -16]!
  stp  w29, w30, [sp, -16]!

  stp  x29, x30, [sp], -16
  stp  w29, w30, [sp], -16


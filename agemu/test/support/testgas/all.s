# ------------------------------------------------------------------------------
# all.s
# Copyright (c) Pau Sanchez - MIT License
# ------------------------------------------------------------------------------
# The purpose of this file is to keep track of all instructions that are
# to be transpiled from python and emulated in C.
# ------------------------------------------------------------------------------
.arch armv8-a
.file  "all.s"
.text

.section .rodata
.align  3
hi_all:
.string  "hi all!"

.section  .text,"ax",@progbits
.align  2
.global  a64_instructions

a64_instructions:

mov_instr:
  mov  w0, w3
  mov  w0, wsp
  mov  x30, sp
  mov  x30, x8

movz_instr:
  # "mov Xn, imm" does not exist
  mov  x30, 32
  movz x30, 32

stp_instr:
  stp  x29, x30, [sp]
  stp  x29, x30, [sp, -16]
  stp  x29, x30, [sp, -16]!
  stp  x29, x30, [sp], -16

  stp  w29, w30, [sp]
  stp  wzr, w30, [sp, -16]
  stp  w29, w30, [sp, -16]!
  stp  w29, w30, [sp], -16

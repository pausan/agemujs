# C Emulator Tests

The aim of these tests is to provide full unit-test coverage for all
instructions, at least, ideally.

## Understanding these tests
This folder contains simple unit tests divided in YAML files for each group
of instructions.

Each test group consists on an test signature, followed by a list of tests for
given signature.

Each test contains input registers, the assembly code to perform (one or more
instructions), and the expected output registers.

Please note that while running the tests, all CPU registers will be initiallized
randomly (apart of SP), and only given output registers will be checked.

Setting up new tests is straighforward. YAML format is used for simplicity.

Example:

    ---- a64_add_like.yaml ---------------------------------------

    test_add_xxx:
      - name : 64-bit zero sum
        in   : { x0 : 0, x1 : 0, x2 : 0}
        out  : { x0 : 0 }
        code : add x0, x1, x2

      - name : another test example, more complex
        in   : { x0 : 0, x1 : 0, x2 : 0}
        out  : { x0 : 0 }
        code : >
          label1:
            # use GAS syntax and as many lines as you want
            add x0, x1, x2
          label2:
            add x0, zr, zr

    --------------------------------------------------------------

**test_add_xxx** is the signature
**name** contains a description of what the test does (optional)
**in** input CPU state before running the code
**code** assembly code to be executed
**out** output CPU state of the flags/registers to be checked

Each code will be independently compiled with **gas**, loaded, transpiled
executed and checked against given input/output, so we not only do the test
but guarantee that the whole code works as expected.

## Input/Output CPU State

You can set 64-bit registers and all flags. 32-bit registers are not allowed.
If you are simulating a 32-bit instruction, just initialize the X register
with the value you want.

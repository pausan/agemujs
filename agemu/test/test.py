#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
import os
import sys
import json
import yaml
import glob
import logging
import tempfile
import random
import shutil
import unittest
import argparse
import subprocess
from pathlib import Path

sys.path.insert(0, os.path.realpath ('../..'))

import testhelper

from agemu import a64disassembler, a64recompiler, elflib
from agemu.a64transpiler import a64cpu

log = logging.getLogger('')

g_subtestFilter = None

class TestTranspilation (unittest.TestCase):
  def setUp(self):
    self.maxDiff = None

    # force a fixed dir or create one randomly
    if unittest.fixed_temp_dir:
      self.tempDir = Path(unittest.fixed_temp_dir)
      os.makedirs(self.tempDir, exist_ok = True)
    else:
      self.tempDir = Path(tempfile.mkdtemp(prefix='agemu-'))

  def tearDown(self):
    """ Cleanup temporary directories on success
    """
    # hack to detect failed tests on python 3.4+
    if (
      ('AssertionError' in str(self._outcome.errors[-1]))
      or ('traceback' in str(self._outcome.errors[-1]))
    ):
      print ("-"*80)
      print (">> Test Failed: Not cleaning up temp dir: ", self.tempDir)
      print ("-"*80)
      return

    if str(self.tempDir) != unittest.fixed_temp_dir:
      shutil.rmtree(self.tempDir)

  def assertSameCpuState(self, cpuState1, cpuState2, ignore_sp = True):
    if isinstance(cpuState1, a64cpu.A64Cpu):
      cpuState1 = cpuState1.toDict()

    if isinstance(cpuState2, a64cpu.A64Cpu):
      cpuState2 = cpuState2.toDict()

    cpu1=cpuState1.copy()
    cpu2=cpuState2.copy()

    if ignore_sp:
      cpu1['sp'] = 0
      cpu2['sp'] = 0
      cpu1.pop('stack_base', None)
      cpu2.pop('stack_base', None)
      cpu1.pop('stack_size', None)
      cpu2.pop('stack_size', None)
    else:
      # TODO: normalize SP for both CPUs
      pass

    self.assertDictEqual (cpu1, cpu2)
    return

  def test001_32bit(self):
    """ Transpile method 'return42_32bits' and check that output processor
    registers have the values they are supposed to have.
    """
    blocks = a64disassembler.transpileObjectSections (
      'support/basic/test001.o',
      self.tempDir,
      symbolsFilter = ['return42_32bits']
    )

    recompiler = a64recompiler.A64Recompiler(blocks)
    recompiler.invoke('return42_32bits')
    recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON')
    #recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

    # compile and test on all architectures
    for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
      program = recompiler.rebuild(self.tempDir, targetArch)
      self.assertTrue (program is not None)

      (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
      self.assertTrue (retcode == 0)
      self.assertTrue (not stderr)

      cpuState = a64cpu.loadCpuStateDict (stdout)
      expectedCpuState = a64cpu.A64Cpu()
      expectedCpuState.x0 = 42

      self.assertSameCpuState (cpuState, expectedCpuState, ignore_sp = True)

    return

  def test001_64bit(self):
    """ Transpile method 'return42_64bits' and check that output processor
    registers have the values they are supposed to have.
    """
    blocks = a64disassembler.transpileObjectSections (
      'support/basic/test001.o',
      self.tempDir,
      symbolsFilter = ['return42_64bits']
    )

    recompiler = a64recompiler.A64Recompiler(blocks)
    recompiler.invoke('return42_64bits')
    recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON')
    #recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

    # compile and test on all architectures
    for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
      program = recompiler.rebuild(self.tempDir, targetArch)
      self.assertTrue (program is not None)

      (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
      self.assertTrue (retcode == 0)
      self.assertTrue (not stderr)

      cpuState = a64cpu.loadCpuStateDict (stdout)
      expectedCpuState = a64cpu.A64Cpu()
      expectedCpuState.x0 = 42

      self.assertSameCpuState (cpuState, expectedCpuState, ignore_sp = True)

    return

  def test001_sum(self):
    """ Transpile method 'sum_64bits' in the optimized and non-optimized version
    to make sure the method is executed properly
    """
    for objectFile in [ 'support/basic/test001.o', 'support/basic/test001_unoptimized.o' ]:
      blocks = a64disassembler.transpileObjectSections (
        objectFile,
        self.tempDir,
        symbolsFilter = ['sum_64bits']
      )

      recompiler = a64recompiler.A64Recompiler(blocks)
      recompiler.invokeRaw('cpu.x0 = 0x100;') # first parameter to sum_64bits
      recompiler.invokeRaw('cpu.x1 = 0x012;') # second parameter to sum_64bits
      recompiler.invoke('sum_64bits')
      recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

      # compile and test on all architectures
      for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
        program = recompiler.rebuild(self.tempDir, targetArch)
        self.assertTrue (program is not None)

        (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
        self.assertTrue (retcode == 0)
        self.assertTrue (not stderr)

        # expected result in X0 (the rest of registers are meaningless, and in
        # fact, get swapped by the compiler in the unoptimized version)
        cpuState = a64cpu.loadCpuStateDict (stdout)
        self.assertEqual (cpuState['x0'], 0x112)

    return

  def test002_sum_two_args(self):
    """ Transpile test002 and perform a call to check that output processor
    registers have the values they are supposed to have.
    """
    blocks = a64disassembler.transpileObjectSections (
      'support/basic/test002.o',
      self.tempDir
    )

    for funcCall in ['sum_32bits', 'sum_64bits']:
      with self.subTest(function = funcCall):
        recompiler = a64recompiler.A64Recompiler(blocks)
        recompiler.invokeRaw('cpu.x0 = 0x100;') # first parameter to sum_32bits
        recompiler.invokeRaw('cpu.x1 = 0x012;') # second parameter to sum_32bits
        recompiler.invoke(funcCall)
        recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

        # compile and test on all architectures
        for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
          program = recompiler.rebuild(self.tempDir, targetArch)
          self.assertTrue (program is not None)

          (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
          self.assertTrue (retcode == 0)
          self.assertTrue (not stderr)

          cpuState = a64cpu.loadCpuStateDict (stdout)
          self.assertEqual (cpuState['x0'], 0x112)

    # invoke sum 42 in 32-bits
    for funcCall in ['sum42_32bits', 'sum42_64bits']:
      with self.subTest(function = funcCall):
        recompiler = a64recompiler.A64Recompiler(blocks)
        recompiler.invokeRaw('cpu.x0 = 0x100;') # first parameter to sum_32bits
        recompiler.invoke(funcCall)
        recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

        # compile and test on all architectures
        for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
          program = recompiler.rebuild(self.tempDir, targetArch)
          self.assertTrue (program is not None)

          (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
          self.assertTrue (retcode == 0)
          self.assertTrue (not stderr)

          cpuState = a64cpu.loadCpuStateDict (stdout)
          self.assertEqual (cpuState['x0'], 0x12a) # 0x100 + 42

    return

  def _compileGasCode (self, name, code):
    """ Compile given code in a GAS compiler for ARM64 arch
    """
    sourceAsmFile = self.tempDir / f'{name}.s'
    objectFile = self.tempDir / f'{name}.o'

    with open(sourceAsmFile, 'wt') as f:
      f.write (f'''
# IMPORTANT: File automatically generated by tests
.arch armv8-a
.file  "{name}.s"
.text

.section  .text,"ax",@progbits
.align   4
.global  start

start:
{ code }
    ''')

    result = subprocess.run(
      ['aarch64-linux-gnu-as', '-o', objectFile, sourceAsmFile],
      cwd = self.tempDir,
      capture_output = True
    )
    if result.returncode != 0:
      raise Exception (f'Problem building test {testId}, file: {sourceAsmFile}. Error: {result.stdout}{result.stderr}')

    return objectFile

  def _runCEmuTest(
    self,
    fileName,
    testId,
    name,
    code,
    inputs,
    outputs,
    **kwargs
  ):
    """ Executes a single test by compiling given assembly code, and transpiling
    it to native and webassembly. It will then initialize emulated CPU with given
    input registers, run the code, and compare with expected output CPU state.
    """
    # prepare input and output values
    cpuInputState = a64cpu.A64Cpu()
    cpuInputState.randomize()
    cpuInputState.setValues (inputs)

    for key, value in outputs.items():
      if value == 'unchanged':
        outputs[key] = cpuInputState.get(key, '!unknown!')

    objectFile = self._compileGasCode(testId, code)

    # TODO: only once to validate tests themselves should be enough, but anyway
    if True:
      testhelper.validateTestInUnicorn(
        fileName,
        testId,
        name,
        objectFile,
        cpuInputState.toDict(),
        outputs
      )

    blocks = a64disassembler.transpileObjectSections (
      objectFile,
      self.tempDir
    )
    if not blocks:
      raise Exception ("Could not generate blocks")

    recompiler = a64recompiler.A64Recompiler(blocks)

    # initialize all registers
    for key, value in cpuInputState.toDict().items():
      if isinstance(value, bool):
        recompiler.invokeRaw(f'cpu.{key} = {"1" if value else "0"};')
      elif isinstance(value, int):
        if value <= 0xFFFF_FFFF:
          recompiler.invokeRaw(f'cpu.{key} = 0x{value:08x};')
        else:
          recompiler.invokeRaw(f'cpu.{key} = 0x{value:016x}ULL;')

    recompiler.invoke('start')
    recompiler.invoke('a64_internal_show_cpu_state', 'OUTPUT_FORMAT_JSON_COMPACT')

    # compile and test on all architectures
    for targetArch in [recompiler.ARCH_WASM, recompiler.ARCH_NATIVE]:
      program = recompiler.rebuild(
        self.tempDir,
        targetArch,
        baseName = testId
      )
      self.assertTrue (program is not None)

      (retcode, stdout, stderr) = recompiler.runProgram(program, targetArch)
      self.assertTrue (retcode == 0)
      self.assertTrue (not stderr)

      # expected result in X0 (the rest of registers are meaningless, and in
      # fact, get swapped by the compiler in the unoptimized version)
      cpuState = a64cpu.loadCpuStateDict (stdout)

      expectedCpuState = outputs
      for key, value in expectedCpuState.items():
        self.assertEqual (
          value,
          cpuState.get(key, None),
          f'test value {key}={cpuState.get(key)} (expecting {value}), on test {testId}.{name} (file={fileName})'
        )

    return


  def testcemu(self):
    """ C Emulation tests. Unit tests for all C emulation instructions.

    Please read support/cemutests/README.md for a longer explanation.
    """
    allTestFiles = glob.glob("./support/cemutests/**/*.yaml", recursive = True)

    for testFile in allTestFiles:
      fileName = os.path.basename (testFile)

      with open(testFile, 'rt') as file:
        testContents = yaml.load(file, Loader=yaml.FullLoader)

        for testId, testList in testContents.items():
          for (testIdx, testDict) in enumerate(testList):
            testName = testDict.get('name', 'index=%d [no-name]' % (testIdx+1))
            testDict['name'] = testName
            fullTestName = f'{testId}-{testIdx}: {testName}'

            if g_subtestFilter and g_subtestFilter not in fullTestName:
              log.debug (f'Skipping test {fullTestName}')
              continue

            with self.subTest(file=fileName, testId=testId, name=testName):
              if 'inputs' not in testDict:
                testDict['inputs'] = testDict.get ('in', {})

              if 'outputs' not in testDict:
                testDict['outputs'] = testDict.get ('out', {})

              log.info (f'Running {fullTestName}...')
              self._runCEmuTest(fileName, f'{testId}-{testIdx}', **testDict)

    return

def initLogging(verbose):
  """ Initialize logging
  """
  #formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
  formatter = logging.Formatter('[INFO] %(message)s')
  ch = logging.StreamHandler()
  ch.setFormatter(formatter)
  log.addHandler(ch)

  if verbose:
    log.setLevel (logging.DEBUG)
  else:
    log.setLevel (logging.INFO)

  return

def parseArgs():
  """ Custom parser to help specify test dir for creating & debugging tests
  and intermediate files
  """
  parser = argparse.ArgumentParser(add_help = False)
  parser.add_argument('-h', '--help', action = 'store_true')
  parser.add_argument('-v', '--verbose', action = 'store_true', help='verbose output')
  parser.add_argument('--seed', help='initialize seed')
  parser.add_argument('--temp-dir', help='specify a fixed temp directory')
  parser.add_argument('--subtest', help='run subtests that match given string (for testcemu and similar. E.g add_www-2)')

  parsed, args = parser.parse_known_args(namespace=unittest)
  sys.argv[:] = sys.argv[:1] + args

  if parsed.help:
    parser.print_help()
    print ("\n" + ("-"*80) + "\n")
    sys.argv.append ('--help')

  if parsed.verbose:
    initLogging(True)
    sys.argv.append ('--verbose')

  if parsed.seed:
    parsed.seed = int(parsed.seed)
  else:
    parsed.seed = random.randint(0, 0xFFFF_FFFF)

  print (f'# --seed {parsed.seed}')
  random.seed (parsed.seed)

  return parsed

if __name__ == '__main__':
  args = parseArgs()
  if args.temp_dir:
    unittest.fixed_temp_dir = os.path.normpath (os.path.join(os.getcwd(), args.temp_dir))
  else:
    unittest.fixed_temp_dir = None

  if args.subtest:
    g_subtestFilter = args.subtest

  unittest.main()


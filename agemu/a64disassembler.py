#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#

import os
import sys
import logging

import capstone
from capstone import arm64 as csarm64

from . import elflib
from .a64transpiler import a64transpiler

log = logging.getLogger('')

def transpileLibrary(lib, outdir):
  """
  """
  log.info (f"Transpiling Library {lib}")

  with open(lib, 'rb') as f:
    elf = elflib.ELFFile(f)
    for section in elf.iter_sections():
      print(hex(section['sh_addr']), section.name)
      # try:
      #   for x in section.iter_symbols():
      #     print (x.entry, x.name)
      # except:
      #   pass
    code = elf.get_section_by_name('.text')

    # symtab = elf.get_section_by_name('.symtab')
    # for x in symtab.iter_symbols():
    #   print (x.entry, x.name)
    return

    ops = code.data()
    addr = code['sh_addr']

    md = capstone.Cs(capstone.CS_ARCH_ARM64, capstone.CS_MODE_ARM)
    md.detail = True

    print ("Data range [%08x, %08x]: %d bytes" % (addr, addr+len(ops), len(ops)))

    for instr in md.disasm(ops, addr):
      showInstrEx(instr)
      continue

def transpileObject(objectFilePath, outdir, verbose = True):
  """ Transpile given object file. The advantage of object files is that are
  much simpler to parse & transform, thus, easier to develop the initial
  transpiler.

  Returns a A64Block list of items with the transpiled object in C.
  """
  return transpileObjectSections (objectFilePath, outdir, verbose, None)

def transpileObjectSections(
  objectFilePath,
  outdir,
  verbose = True,
  symbolsFilter = None
):
  """ Transpile given object file. The advantage of object files is that are
  much simpler to parse & transform, thus, easier to develop the initial
  transpiler.

  _symbolsFilter_ is a list of symbol names to be transpiled. Only sections containing
  those symbols will be transpiled.

  Filtering by symbols is only useful for tests but the rest of the function
  works the same way.

  Returns a A64Block list of items with the transpiled object in C.
  """
  log.info (f"Transpiling Object Partially {objectFilePath}")

  obj = elflib.Object()
  obj.load (objectFilePath)
  obj.disassemble()

  blocks = []
  for section in obj.sections:
    if section.hasCode() and (len(section.instrs) == 0):
      continue

    # filter by given list of symbols (if any)
    if symbolsFilter and not section.symbols.hasAny(symbolsFilter):
      continue

    if section.hasCode():
      block = a64transpiler.transpileFunction (section, verbose)
    else:
      block = a64transpiler.transpileData (section, verbose)

    if block:
      blocks.append (block)

  return blocks

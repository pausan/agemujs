#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Pau Sanchez - MIT License
#
__author__ = "Pau Sanchez"
__copyright__ = "Copyright (c) 2019 Pau Sanchez"
__credits__ = []
__license__ = "MIT"

import os
import sys
import argparse
import logging
import zipfile
from pathlib import Path

from agemu import a64disassembler
from agemu import a64recompiler

log = logging.getLogger('')

RAW_APK_DIR = '_rawapk'

def initLogging(verbose):
  """ Initialize logging
  """
  #formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
  formatter = logging.Formatter('%(message)s')
  ch = logging.StreamHandler()
  ch.setFormatter(formatter)
  log.addHandler(ch)

  if verbose:
    log.setLevel (logging.DEBUG)
  else:
    log.setLevel (logging.INFO)

  return

def transpileDalvik (dalvikFile, outdir):
  # TODO: to be implemented
  pass

def transpileLibrary(lib, outdir):
  """
  """
  a64disassembler.transpileLibrary (lib, outdir)

def transpileObject(objectFilePath, entryPoint, outdir, verbose):
  """ Transpile object file and dump to stdout
  """
  blocks = a64disassembler.transpileObject (objectFilePath, outdir)
  recompiler = a64recompiler.A64Recompiler(blocks)
  recompiler.invoke(entryPoint)
  recompiler.dumpCCode(sys.stdout, verbose)
  return

def disassembleObject(objectFilePath):
  """ Disasemble object file and dump to stdout
  """
  from agemu import elflib
  obj = elflib.Object()
  obj.load (objectFilePath)
  obj.disassemble()
  print(obj)
  return

def transpileLibraries(libs, outdir):
  for lib in libs:
    transpileLibrary(lib, outdir)

def transform(apkfile, outdir):
  """ Transform given APK and saves all output files to outdir
  """
  log.debug (f"Processing {apkfile}")

  # uncompress APK to disk
  rawApkDir = outdir / RAW_APK_DIR
  os.makedirs(rawApkDir, exist_ok = True)
  with zipfile.ZipFile(apkfile) as apk:
    apk.extractall (path = rawApkDir)

  # check meaningful files first
  arm64dir = rawApkDir / 'lib/arm64-v8a'
  manifest = rawApkDir / 'AndroidManifest.xml'
  dalvik = rawApkDir / 'classes.dex'
  try:
    libs = [x for x in arm64dir.iterdir() if x.is_file() ]
  except:
    log.error ("No AARCH64 libraries in lib/arm64-v8a folder")
    return False

  if not manifest.is_file():
    log.error ("Missing AndroidManifest.xml")
    return False

  if not dalvik.is_file():
    log.error ("Missing classes.dex")
    return False

  if not libs:
    log.error ("No AARCH64 libraries in lib/arm64-v8a folder")
    return False

  transpileDalvik (dalvik, outdir)
  transpileLibraries (libs, outdir)

  # TODO:
  #   - cleanup APK in disk and other files, unless --preserve-intermediate-files

  return True

def main ():
  """ processes parameters & runs the show
  """
  parser = argparse.ArgumentParser(
    description='Tool used to transform APK into something runnable on the web'
  )

  parser.add_argument('-d', '--output-dir', default="webout", help='output folder where to save all transformed files')
  parser.add_argument('--entry-point', default="entry_point", help='transpilation entry point')
  parser.add_argument('--transpile-object', action="store_true", help='transpile object file')
  parser.add_argument('--disassemble-object', action="store_true", help='disassemble object file')
  parser.add_argument('-v', '--verbose', action="store_true", help='shows verbose information')
  parser.add_argument('file', help='apk file to be transformed')

  args = parser.parse_args()

  initLogging (args.verbose)

  if args.transpile_object:
    ok = transpileObject(
      args.file,
      args.entry_point,
      Path(args.output_dir),
      args.verbose
    )

  elif args.disassemble_object:
    ok = disassembleObject(args.file)

  else:
    ok = transform(args.file, Path(args.output_dir))

  return 0 if ok else -1

if __name__ == '__main__':
  sys.exit(main())
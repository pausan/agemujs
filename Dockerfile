FROM ubuntu:19.10

RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y \
            unzip \
            vim \
            curl \
            cmake \
            python3 \
            python3-pip \
            python3-yaml

# emscripten requirements
RUN apt-get install -y \
            python \
            python2.7 \
            cmake \
            default-jre

RUN curl -L -o /tmp/emscripten.zip \
            https://github.com/emscripten-core/emsdk/archive/master.zip

RUN unzip -d /usr/lib/local/ /tmp/emscripten.zip
RUN /usr/lib/local/emsdk-master/emsdk install latest
RUN /usr/lib/local/emsdk-master/emsdk activate latest
RUN /usr/lib/local/emsdk-master/emsdk_env.sh

RUN curl https://get.wasmer.io -sSfL | sh

# installing for PIP because packages would be more recent
RUN pip3 install pyelftools capstone unicorn

COPY docker/entrypoint.sh /usr/lib/agemujs/docker/entrypoint.sh
COPY *.py /usr/lib/agemujs/
COPY agemu /usr/lib/agemujs/agemu

WORKDIR /usr/lib/agemujs/

ENTRYPOINT ["/usr/lib/agemujs/docker/entrypoint.sh"]

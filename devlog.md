# Development Log

A simple log to share the progress on the app. Simpler than setting up a blog
and it's easy and convenient to search for ideas or thoughts.

This project is open to anybody that wants to participate.

## 2019-12-08 Addressing modes in ARM load/store instructions

There are four addressing modes for load and store operations:

  - **Base register:** e.g: load the contents of X1 in W0

        LDR  W0,  [X1]

  - **Offset addressing:**  e.g: load the contents of (X1+12) in W0

        LDR  W0,  [X1, #12]

  - **Pre-index addressing:**  e.g: load the contents of (X1+12) in W0 and
    set X1 = X1 + 12. The addition is performed BEFORE loading memory contents.

        LDR  W0,  [X1, #12]!

  - **Post-index addressing:**  e.g: load the contents of X1 in W0 and
    set X1 = X1 + 12. The addition is performed AFTER loading memory contents.

        LDR  W0,  [X1], #12

I think the assembly code is kind of tricky, but it is easy to spot which
is which.

Had I been the designer, I would have probably chosen something in the line of:

        LDR  W0,  [X1]
        LDR  W0,  [X1 + 12]
        LDR  W0,  [X1 += 12]
        LDR  W0,  [X1], X1 += #12

I believe this would be more clear, but anyway, I'll get used to it.

A better explanation can be found here:

  - https://developer.arm.com/architectures/learn-the-architecture/armv8-a-instruction-set-architecture/loads-and-stores-addressing

## 2019-12-07 Compile hello world to ARM64 and run it in the emulator: Part I

After discovering the issues with emscripten and wasmer and moving to **puts**
and **printf** instead of **write**, I wrote a simple hello world method and
compiled it to ARM64.

    void say_hi(void) {
      puts("Hi!");
    }

The AARCH64 code produced is:

    stp     x29, x30, [sp, #-0x10]!
    mov     x29, sp
    adrp    x0, #0                   rel[00000000: .rodata  (+0) [275]]
    add     x0, x0, #0               rel[00000000: .rodata  (+0) [277]]
    bl      #0x10                    rel[00000000: puts     (+0) [283]]
    nop
    ldp     x29, x30, [sp], #0x10
    ret

Apart of the new instructions that need to be implemented in the c-side in
order for the transpiled/emulated code to work, there are two references:

- A reference to **internal data** (the "Hi!\0" string)
- A reference to the **puts** method.

So I need to implement several things on this phase:

- Emit read-only data to be recompiled again (**"Hi!\0"**)
- Calls to external "known" functions (**puts**)
- A new bunch of instructions:
  **strp, mov, adrp, add, bl, nop, ldp**

From my current understanding, I've just spotted the very first potential
optimization pattern.

The code is using **adrp** and **add** to put in **x0** the 64-bit
location of **"Hi!"** string. This happens because, unlike in x86/x64, ARM64
opcodes have a fixed length of 4 bytes, so there is no way of encoding 64-bit
integers in 4 bytes, so compilers need to do some tricks. The optimization
would be to emit something like **x0 = hi_ptr** where hi_ptr = "Hi!".

For now I'm going to make it work without optimizing or creating virtual
instructions.

## 2019-12-07 Compile hello world with emscripten and wasmer: FAIL!

My thought was: let's build a simple hello world function, compile it to ARM,
then transpile it to native & wasm, and use this as a base point to build
more methods and tests (in order to perform calculations and compare outputs).

It turns out, the challenge is bigger than I though, not because the emulation
of the ARM instructions, but because even compiling to webassembly and running
with emscripten and wasmer does not work. Let me show you.

This is my C code:

    #include <unistd.h>

    void say_hi(void) {
      write(1, "Hi!\n", 4);
    }

    int main() {
      say_hi();
      return 0;
    }

I then execute the following commands (emscipten 1.39.4, wasmer 0.11.0):

    $ emcc -o hi.wasm hi.c
    $ wasmer hi.wasm
    Error: Can't instantiate module: LinkError([ImportNotFound { namespace: "wasi_unstable", name: "proc_exit" }, ImportNotFound { namespace: "env", name: "__syscall4" }])

I've been playing a little bit with the emcripten and wasmer parameters, but
everything failed. After reading a little bit it turns out that while emscripten
is in the way of supporting WASI (Web Assembly System Interface), it is not there
yet.

I downloaded wasienv, which is another environment (WASI-compliant), and tried
again. Guess what:

    $ wasicc -o hi.wasm hi.c
    $ wasmer hi.wasm
    Hi!

So yes... emscripten is not yet supporting this use case... I'm sure it will be
supported in the future, but for now I have two options. Move to wasienv to run
the tests or use 'puts' or 'printf' instead of the plain 'write' syscall.

I've decided to stick with emscripten for now, so I'll be running the tests
with stdio calls instead, which seem to work.

The following input file runs well in wasmer after compiling both with
**wasicc** and **emcc**:

    #include <unistd.h>
    #include <stdio.h>

    void say_hi(void) {
      puts("Hi!");
    }

    int main() {
      say_hi();
      return 0;
    }

References:

  - https://github.com/wasienv/wasienv

## 2019-12-07 Compile to webassembly & run in console

To compile generated code to webassembly & run it locally in the console there
are several options (see references). Basically both clang and emscripten would
do, although emscripten has a wider purpose.

Since emscripten can produce standalone wasm code that can be executed directly
by a webassembly runtime, it makes sense to start using it, even if I'm going
to start compiling tiny examples.

Now, regarding code execution, there are several webassembly runtimes, like
wasmtime, wasmer, wavm, lucet, ... Each one has its pros/cons. I'm sure more will
appear.

Anyway, while this might change in the future, I see two good fits for testing
the project. wavm, since it seems to be really fast, and wasmer, since it seems
the most portable.

For me, portability is more important now, so I decided to move ahead with wasmer.
Maybe I'll add some benchmarks with wavm in the future.

I've just done minor updates to the code in order to run the test I finished
yesterday both with native mode as with emscripten and wasm. It works! :D

References:

- https://v8.dev/blog/emscripten-standalone-wasm
- https://dassur.ma/things/c-to-webassembly/
- https://wasmer.io/
- https://wasmtime.dev/
- https://github.com/WAVM/WAVM
- https://github.com/bytecodealliance/lucet
- https://00f.net/2019/10/22/updated-webassembly-benchmark/
- https://dassur.ma/things/c-to-webassembly/

## 2019-12-06 Emulating first two instructions

While the original aim is to compile to webassembly, I decided to start by building
a small test that transpiles a simple function to native, and runs it.

I had to build a lot of boilerplate code, but I belive it will pay off shortly.

The test also prints the ARM64 CPU state, with all registers and flags, so it can
be compared to the expected output.

The original C code:

    uint32_t return42_32bits(void) {
      return 42;
    }

Compiled ARM64 code, with LLVM is:

    return42_32bits:
      movz    w0, #0x2a
      ret

Transpiled code to C:

    void return42_32bits (arm64cpu_t *ctx) {
      a64_movz_regw_imm (ctx, &(ctx->r0), 0x0000002a, 0 );
      return;
    }

Compiled to native x86 code:

    return42_32bits:
      endbr64
      mov    QWORD PTR [rdi],0x2a
      ret

The output of the program:

    {
       "x0" : 0x00000000_0000002a,
       "x1" : 0x00000000_00000000,
       ...
      "pstate_overflow" : false,
      "pstate_zero"     : false,
      "pstate_carry"    : false,
      "pstate_negative" : false
    }

While I'm not sure the current movz does all it needs to do... it works!

Next I would like to some of the following things:

1) compile to webassembly and find a way to run it in the console.

2) use unicorn engine to randomly initialize the cpu state and be able to run
some code snippets both with my emulator and with unicorn engine. Hopefully
both should produce same results. This way I can also test random instructions.

3) build some programs that write to standard output, compile them in native
and in ARM64 and run them both. If things work as expected, they should produce
same output. This would be great for some black-box tests.

## 2019-12-05 Transpile basic instructions and refactor

Since the object file has symbols to the start of each method, first thing
would be to just transpile each method.

From the point of view of an optimal conversion, the methods should be seen
as a black box where you have some input parameters (from registers and/or stack),
and some outputs (return value or values). In the middle there can be other
calls and memory can be modified. Ideally the stack should be not modified
but of course, optimizations might happen in a bigger program...

First step would be to convert ARM64 assembler into C code. Let's start with
the premise that at some point there will be a module that can identify functions
and that functions only have one entry point at the beginning... Not sure for
how long this premise will hold true, but for now will do.

I've implemented some methods and done some refactoring to keep stuff as clean
as possible for now.

I've also changed the way the test code is being generated now so all C functions
from the object file are each on a separate section, that way I can test stuff
better.

Next step will be to generate real C code that can compile and be executed,
and probably test it against an existing emulator like unicorn so I can compare
that the output is the same.

Useful information:

  - https://developer.arm.com/architectures/learn-the-architecture
  - https://developer.arm.com/architectures/learn-the-architecture/armv8-a-instruction-set-architecture
  - https://modexp.wordpress.com/2018/10/30/arm64-assembly/

### Notes from ARM documentation:

- Each register can be used as a 64-bit X register (X0..X30), or as a
32-bit W register (W0..W30). These are two separate ways of looking at
the same register. For example, this register diagram shows that W0 is
the bottom 32 bits of X0, and W1 is the bottom 32 bits of X1

- The choice of X or W determines the size of the operation:

    ADD W0, W1, W2  ; 32-bit integer addition
    ADD X0, X1, X2  ; 64-bit integer addition

AARCH64 calling convention:
- **x0 to x7**: argument registers and to return a result. caller-saved register
variables that can hold intermediate values within a function or between calls
to other functions.
- **x8**: indirect result register. This is used to pass the address location of an
indirect result, for example, where a function returns a large structure.
- **x9 to x15**: hold local variables (caller saved). They can be modified by the
called subroutine
- **x16 and x17** X16 and X17 are IP0 and IP1, intra-procedure-call temporary registers.
These can be used by call veneers and similar code, or as temporary registers for
intermediate values between subroutine calls. They are corruptible by a function.
Veneers are small pieces of code which are automatically inserted by the linker,
for example when the branch target is out of range of the branch instruction.
- **x18:** is the 'platform register', used for some operating-system-specific
special purpose, or an additional caller-saved register
- **x19 to x29**: callee-saved registers. hey can be modified by the called
subroutine as long as they are saved and restored before returning.
- **x29** is the frame register (FP)
- **x30** is the link register (LR), used to return from subroutines.
- **xzr**, the zero register, always read as 0 and ignore writes.
- **sp** serves as a stack pointer for loads and stores

Regarding SIMD registers:
- **v0-v7** are used to pass argument values into a subroutine and to return result values from a function. They may also be used to hold intermediate values within a routine (but, in general, only between subroutine calls).
- **v8-v15** must be preserved by a callee across subroutine calls.
Only the bottom 64 bits of each value stored in V8-V15 need to be preserved.
- **v16-v31** do not need to be preserved (or should be preserved by the caller).

Memory:
- Always zero-extend or sign-extend (to the dest size, if W, extend to 0 the X high part)
- Remember that in AArch64 the stack-pointer must be 128-bit aligned.

Flow:
- The following are equivalent, although RET is better for branch prediction:
    RET
    BR LR
    BR X30

Read more:

- https://developer.arm.com/architectures/learn-the-architecture
- https://developer.arm.com/solutions/graphics/resources/tutorials/64-bit-android-development
- https://developer.arm.com/architectures/instruction-sets/simd-isas/neon/intrinsics
- https://developer.arm.com/architectures/instruction-sets/simd-isas/neon/neon-programmers-guide-for-armv8-a
- https://en.wikipedia.org/wiki/Calling_convention
- https://developer.arm.com/architectures/learn-the-architecture/armv8-a-instruction-set-architecture/procedure-call-standard

## 2019-12-01 Playing with ELF Files and AARCH64 disassembly

I decided to go with Python to program the main transpiler (so to say). The reason
to go with Python is because, even though it might be way slower than say C, C++,
Rust or Go, it is really easy and *fast to iterate* with it.

Also the *slowness* of transforming a potentially big APK into something different
does not worry me. Potentially you only have to do that once for each app, so
it does not really matter if we are talking about 6 seconds or 60. If needed,
though, we can always use cython/nuitka or implement custom code in C to speed
up the slow parts.

There are two libraries that I decided to start with. *Capstone* and *elftools*.

**Capstone** looks like the perfect fit for this project. Not only provides a
generic disassembler, but it also provides extra information about implicit
registers or flags being read or written by the instruction, which might also
help in order to try to analyze, transform and optimize instructions.

**elftools** seems a good library to read information about ELF files. I only
need to read, not write, so also a good fit.

I've started implementing *agemutool.py* to unpack an APK and start analyzing the
AARCH64 libraries that come with it. The thing is that after compiling a simple
android project (*android/src/dummy01*), with a simple JNI method, I've seen
that the .so is huge!! I presume a lot of the standard C++ library is linked
there. So it is a no-go for a proof of concept implementation.

I've changed the approach not only to avoid compiling a binary executable file,
but to start with a simple object file. For that reason, what I decided to do
is to create a *agemu/test/test001.c* with some simple functions. The idea
is to do something that loads that file (in ELF format) and transforms it
into something that can be ran in wasm/WebAssembly/browser.

I've created **elflib** python module as a wrapper over *elftools*. elftools
will make it easier to myself to manipulate ELF object files, sections,
symbols, relocations and dissasembled code.

*elflib* is still work in progress.

Hopefully next step, once *elflib* is done, would be to try to transform and
run the AARCH64 code in the object file using a wasm interpreter.

One of the nice accomplishments today, is that elflib is already capable
of showing a nice output from an object file with disassembled code already:

    $ python3 aarch64transpiler.py test/test001/test001.o
    [...]
    Section 001: .text
      type      : SHT_PROGBITS
      flags     : 6 ['alloc', 'execinstr']
      addr      : 0
      offset    : 64
      size      : 160
      link      : 0
      info      : 0
      addralign : 4
      entsize   : 0
      symbols   :

        0x00000000:                                  [STB_LOCAL, STT_SECTION, STV_DEFAULT]
        0x00000000: $x                               [STB_LOCAL, STT_NOTYPE, STV_DEFAULT]
        0x00000000: return42_32bits                  [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000008: return42_64bits                  [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000010: sum42_32bits                     [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000028: sum_32bits                       [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000048: sum_64bits                       [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000068: callsome                         [STB_GLOBAL, STT_FUNC, STV_DEFAULT]

      code      :

        0x00000000:                                  [STB_LOCAL, STT_SECTION, STV_DEFAULT]
        0x00000000: $x                               [STB_LOCAL, STT_NOTYPE, STV_DEFAULT]
        0x00000000: return42_32bits                  [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000000: 52 80 05 40   movz    w0, #0x2a                           (s=4)
        0x00000004: d6 5f 03 c0   ret                                         (s=4)

        0x00000008: return42_64bits                  [STB_GLOBAL, STT_FUNC, STV_DEFAULT]
        0x00000008: d2 80 05 40   movz    x0, #0x2a                           (s=4)
        0x0000000c: d6 5f 03 c0   ret                                         (s=4)

    [...]

Read more:
  - http://refspecs.linuxbase.org/elf/gabi4+/contents.html
  - https://blog.k3170makan.com/2018/09/introduction-to-elf-format-elf-header.html

## 2019-11-29 AGEMU Starts

Long story short, I've been thinking and playing in my head for a couple of
years about starting this project. Today is the day I decided to start it.

It has taken me an hour to come up with the name of the project. Why naming
stuff is always so hard? Anyway, it is probably not the best name in the world,
ut since I'd like to focus on emulating games and not generic dalvik
applications, seems good enough name to start with.

I have plenty of experience in disassembling and doing nasty things to x86/x64
code in Windows, for that reason I know how hard it is to properly disassemble
it and/or try to manipulate it without source code / object files.

I also have good experience with disassembling and transforming dalvik code.
I would say it is many orders of magnitude easier to hack and play with than
x86/x64. Dalvik code does not worry me at the moment, but I'm sure emulating some
of the Android methods will be a lot of work to get right.

On the other hand, AARCH64 is something new to me. Other than dalvik and x86/x64,
I have no experience. I like the fact that even though AARCH64 processor can run
ARM32 and THUMB/THUMB2 code, it can't switch modes if it is running AARCH64, so
the project will only have to deal with ARM64/AARCH64 instruction set. It makes
things a little bit easier.

Unlike PE/COFF, ELF is new to me. Good thing is that many of the concepts I
presume would be the same or very similar to those of PE/COFF, but will take
me a while to grasp and understand all the bits of it (e.g: I still don't
quite understand the difference between sections and segments).

Sometime ago I did some performance tests on WebAssembly and I have to say that
speeds were reasonably good compared to native. It will never beat native speeds,
though. As long as the Web provides good API wrappers, I believe some good stuff
can be done (like WebGl, ...).

The idea of transforming an android game to run on the browser sounds tricky.
Being able to play a game with good performance is tricky already with the
source code, so without it, and having to transform it to an intermediate
bytecode (such as WebAssembly) might or might not be accomplished in this project,
but it sounds to me like it is going to be a fun project where at least I'm
going to learn a lot.

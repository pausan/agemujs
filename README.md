# Android Games Emulator in Web Assembly

Project to explore the possibilities of emulating AARCH64/ARM64 code in a browser
using webassembly. Name is now more exciting than what the project really does
( this project only disassembles some bunch of instructions for now ).

This idea comes from the fact that webassembly is capable of running some games
on a browser compiled with emscripten or similar... so why not try? even if only
for fun, and only scratching the surface, it should be a good learning experience.

This is just a project done for fun and learning on my free time. You can check
status to see how far I've got. Feel free to join :)

Read [Development Log](devlog.md) for more details on the progress.

## Status

Starting to experiment and modeling AARCH64 CPU...

For now the project disassembles object files and transpiles some basic
instructions. ~~Not emulating anything yet.~~ Emulates first 2 instructions! :tada:

## Building & running

Dockerfile has been provided to do the heavy lifting. If you'd like to see
what are the requirements to run it in your system, there is nothing better
than checking what's done in dockerfile.

Use docker-compose in order to build and to run in a straighforward way:

    $ docker-compose build
    $ docker-compose run --rm agemutool --transpile-object agemu/test/support/basic/test001.o
    $ docker-compose run --rm agemutool --disassemble-object agemu/test/support/basic/test001.o
    $ docker-compose run --rm agemutool android/precompiled/dummy01.apk

Current path will be mounted in the volume to make it easy to run the tools
locally.

Otherwise, if you install all dependencies locally, you can run:

    $ python3 agemutool.py --transpile-object agemu/test/support/basic/test001.o

To run the tests (for now):

    $ cd agemu/test
    $ python3 test.py

## Apprach

The idea is not to load the whole Android OS, but to do a program emulator instead.

The complexity of this approach is way higher, but the optimization possibilites
are hopefully higher as well, since we can hook stuff in the middle and have a
fallback for OS libraries, etc...

Biggest issue with Android is that we have to emulate two kind of "processors",
on one hand we have the Dalvik VM and on the other hand the ARM64 libraries,
plus the JNI calls between them.

Crazy though, probably Android's java classpath and classes can be transpiled
into web assembly (Dalvik is already divided in classes, methods, fields, params,
etc... so while it has its complexity, it should be approachable).

On the other hand we have the real AARCH64 libraries/machine code. There,
there are several approaches, but since we can limit the scope to one architecture,
maybe we can either run using an emulator (slow) or try to transpile from aarch64
machine code to webassembly (probably unsolvable on all cases). We might even
have to use a little bit more of memory and use an hybrid approach, like
running transpiled code in webassembly whenever possible, and still have the raw
machine code in memory so that on certain situations we use the emulator. Sort of
a JIT (but not really, because it would be done beforehand). Transpiling to
webassembly should help made some optimizations and reduce the number of
instructions ran by the real processor in order to emulate the ARM64 instruction.
By statically analyzing and transpiling code we can also, hopefully, make some
more optimizations to the code being executed. It will never run at the real
speed, though.

Then we have what's inside the APK, like the binary XML manifest, assets, etc...
but zip files can be prepared beforehand as well and we can create a wrapper
over file system and place some hooks to our own functions instead of calling
an OS and do whatever we want with those. This is the easiest part for sure.

### Transpiling Dalvik to WASM

Nothing done or though yet.

### Transpiling AARCH64 to WASM

Implemented a processor context (e.g all registers & flags).

The idea is to create a script that reads the code from the ELF library
and for each instruction that is found, emit a C function call to a C
implementation of that instruction (plus, hopefully, some optimization params).

Once the machine code has been transformed into C, we can transpile using
emscripten into web assembly. I thought of using LLVM IR, but C is more
portable and I already know how to write code in it, plus it won't change
in different versions of LLVM. I'm trusting LLVM/emscripten to optimize C code.
In the future, if this approach works, we can always transpile C to LLVM IR,
change the code emmiter to produce LLVM IR, and optimize from there.

Data should probably be exported to a UInt8 array in order to be imported in javascript,
so it can be accessed both from webassembly and emulator (if we do hybrid approach).

All references to data should be adjusted accordingly in the transpiled code, somehow.

### Emulating AARCH64

The idea is to reuse same code done in C for transpiling. Hopefully it can be
compiled in a separate module.

### OS / Android Classpath calls

These will need to be either emulated or transformed or hooked somehow...

For OpenGL it is clear we need to transform it to WebGL, and for file system we
need to create a virtualization layer somehow... but then we have threading
calls, calls to ICU or other libraries, etc...

### Gluing everything together

Python will be used for the static code generation phase. Will allow faster
development iterations and it is easy to work with it and reason about it.

Since this process will be repeated many times during development phase,
I'm better spending my time developing than compiling.

### Workflow

Python script *xxx* should load an APK file and produces an output dir with
all the files needed in order to run it in the browser.

APK file is unzipped and classes.dex will be processed by *yyy* and transpiled
to *yyy2*, then lib/arm64-v8a/*.so are decompiled by *zzz* and the output
compiled back to *zzz2*.

Extra libraries or dependencies are either embedded on the output files or
added to the final output dir.

### Thoughts

In aarch64 (aka arm64) you cannot mix 64-bit code with 32-bit code in the same
application, thus not making possible to launch a 64-bit application that loads
a 32-bit library (or vice versa). Execution state can only change on exception
entry or return, on the kernel.

This is the main reason why we I am only implementing AARCH64 architecture and
forgetting about ARM32 and THUMB, even though 64-bit ARM processor would be
able to run such applications, I'm only focusing on 64-bit applications instead.
Limiting the scope reduces the amount of effort I have to put in.

## Structure

The code is divided into a python app and C libraries.

Python application will be in charge of transforming APK into something else
and convert/transpile ARM64 and Dalvik code into C.

There are some C libraries that exist to emulate dalvik and ARM64 instructions
and are used to be compiled along produced C code. Take into account that all
instructions will be inlined so that the compiler can optimize stuff.

The main actors are:

* agemutool.py
  This is the main tool to drive the whole transpilation process.

* agemu/elflib
  Wrapper over

* agemu/a64disassembler
  Contains the code to disassemble libraries and object files (used for tests)
  and all the high-level logic to infer functions, ...

* agemu/a64transpiler
  Contains all ARM64 instructions and has the information about how to transpile
  them into C.

* agemu/a64transpiler/ccode
  The C code that will be used in order to make transpiled code to compile by
  a real LLVM C compiler.

## Thanks

Special thanks to the people who built Capstone Engine, pyelftools,
Unicorn Engine and fcd.

Thanks as well to all the people who create tutorials, guides or websites
referenced throught this project.

Last but not least, to the people who designed dalvik and ARM64 and the ones
who created the technical guides I've been reading lately.

### Links

ARM Cortex-A Series Programmer’s Guide for ARMv8-A Documentation
https://developer.arm.com/docs/den0024/a/preface

### Awesome projects to learn form

* http://www.capstone-engine.org/
* http://www.unicorn-engine.org/
* http://zneak.github.io/fcd/
* https://github.com/eliben/pyelftools

## Copyright & License

MIT License

Copyright (c) 2019 Pau Sanchez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Copyright Warning

Be aware that internal libraries used by this project might use other licenses.

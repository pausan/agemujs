#!/bin/bash
#
# Helper file to make emscripten and wasmer environment variables available
# and be able to execute them within docker container.
#

# Make wasmer & emscripten available by silently setting env vars
source /root/.wasmer/wasmer.sh                  &> /dev/null
source /usr/lib/local/emsdk-master/emsdk_env.sh &> /dev/null

# no parameters passed
if [[ $# -eq 0 ]]; then
  python3 /usr/lib/agemujs/agemutool.py --help
  exit $?
fi

# starts with '-' like a paramter to agemutool
if [[ "$1" == "-"* ]]; then
  python3 /usr/lib/agemujs/agemutool.py $@
  exit $?
fi

# is an executable file?
if [[ -x `which $1` ]]; then
  exec "$@"
  exit $?
fi

# finally, if neither of the above, try run whatever that has been passed
# as parameter
python3 /usr/lib/agemujs/agemutool.py $@
